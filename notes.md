# Notes for distribution sensitive secure (DSSS) sketch
* Take error distribution and message distribution into account
  - define ETC and ETD
  -    Secure Sketch
  - HE Scheme, HE + typo detection, Security of ETC
	
## Fuzzy Crypto
By __taking message distro into account__ we can do much better

### Contribution
   - Natural distribution 


### TODO: READ
   - Reed Solomon ECC
   - BCH




## MTurk
 - Caps lock on or off = DONE
 - Check browser compatibility = DONE
 - Is keyboard a problem = Dont think


####  Confusing key errors
- '0' and 'o' or 'O'
- 'l', 'I',  '1' and '|'


### Result of Mturk Experiment first level
   * General keyboard: (total errors 1851)
	 - case typo: 632 typos only differ by case out of 1851.
	 - keyboard close typos: 237
   * Mobile Keyboard: (total errors 258)
	 - keyboard close typo: 98
	 - case typo: 25

#### Edit distance embedding onto hamming distance.
   - [This paper](http://iuuk.mff.cuni.cz/~koucky/papers/editDistance.pdf) talks about embedding edit distance into
   hammming distance space. Seems interesting!
   - 'password'  --> ppaaassswoorrrd
   - 'pasword'   --> ppaaasswoorrrd


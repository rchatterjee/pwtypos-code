# import mturk.mturk_common
import sys, os
# quickscripts.fix_user_agent()
func, arguments = sys.argv[1], sys.argv[2:]
if func=='-gplot':
    from security import evaluate_and_plot
    # evaluate_and_plot.plot(*sys.argv[2:])
    # evaluate_and_plot.test_evaluate_guesses(*sys.argv[2:])
    # evaluate_and_plot.process_guess_files(sys.argv[2:])
    evaluate_and_plot.calculate_sec_vs_typo(sys.argv[2])

elif func=='-plot':
    from plots import plot_statistics
    # plot_statistics.edist_vs_typo()
    # plot_statistics.turker_vs_typo()
    # plot_statistics.complexity_vs_typo()
    # plot_statistics.length_vs_pwtypo(bar_chart=True)
    plot_statistics.typing_speed_vs_typo()
elif func=='-guess':
    from security.compute_guesses import find_all_guesses
    # evaluate_and_plot.plot(*sys.argv[2:])
    # config.POLICY = int(sys.argv[2])
    find_all_guesses(real_model_leak='phpbb', attacker_model_leak='phpbb', policy=2)
    # find_best_guesses.find_all_guesses(*sys.argv[3:])

elif func=='-guesstop3allchecker':
    from security.compute_guesses import compute_guesses_for_all_strategies, secloss_with_q
    for leakname in ['rockyou', 'myspace', 'phpbb']:
        compute_guesses_for_all_strategies(leakname)
    #secloss_with_q(sys.argv[2])

elif func.startswith('-mturk'):
    from mturk import mturk_create_hit, mturk_utils
    # mturk_create_hit.create_hit_for('GENERAL', test=False)
    mturk_utils.download_results()
    # mturk_create_hit.update_hittype_of_remaining('GENERAL')

elif func.startswith('-db'):
    from typodata import quickscripts
    # quickscripts.fix_pwstatus()
    quickscripts.insert_complex_pw()

elif func.startswith('-edits'):
    from edits import edits
    # edits.test_AuthServer()
    e = edits.Edits()

elif func == '-modify':
    edits = ['same', 'swc-all', 'rm-lasts']
    from edits.edits import Edits, AuthServer
    e = Edits()
    a = AuthServer(edits, 1)
    print a.check('december')

elif func.startswith('-edits'):
    from edits import edits
    edits.main()

elif func=='-typochecker':
    from security.compute_guesses import (compute_guesses_for_all_strategies, 
                                          glist2deltaq, find_all_guesses)
    from multiprocessing import Pool
    #glist2deltaq('weirpcfg')
    #glist2deltaq('phpbb')
    # glist2deltaq('myspace')
    # pool = Pool()
    # pool.map(compute_guesses_for_all_strategies, ['rockyou', 'myspace', 'phpbb', 'weirpcfg'])
    #compute_guesses_for_all_strategies('rockyou')
    # compute_guesses_for_all_strategies('myspace')
    # compute_guesses_for_all_strategies('phpbb')
    compute_guesses_for_all_strategies('weirpcfg')

elif func=='-typostat':
    from typoanalysis import typo_types as ttype
    from edits.edits import Edits
    e = Edits()
    #ttype.typo_fix_stat()
    # ttype.test_tab_key()
    ttype.test_cmu_data_typos()
    ttype.test_cmu_data_failed_logins()

elif func=='-authserver':
    from security.pwmodel import PWModel
    from edits.authserver import get_qth_most_prob_pw
    pwm = PWModel(leakname='rockyou', evaluation_model=True)
    print get_qth_most_prob_pw(pwm, 1000)

elif func=='-heatmap':
    from typoanalysis import generate_typo_model
    generate_typo_model.create_heat_map()

elif func == '-rand':
    from edits.authserver import AuthServerRandomized
    allowed_edits = ['same', 'swc-all', 'swc-first', 'rm-lastc',
                'rm-firstc', 'sws-last1', 'upncap',
                'add1-last']
    #pwmodel = PWModel(leakname='rockyou', pw_filter=config.PW_FILTER)
    aserver = AuthServerRandomized(allowed_edits, policy_num=1, little_k = 200, pwmodel=None)
    print aserver.typo_fix_rate();
    
else:
    import importlib
    # import typodata, plots, security, edits
    # from typodata import dbaccess
    module = sys.argv[1].replace('/','.').replace('.py', '')
    func = sys.argv[2]
    A = getattr(__import__(module, 
                            fromlist=[func]), 
                func)
    #A()
    eval('A(*{})'.format(sys.argv[3:]))

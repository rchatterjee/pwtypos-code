from math import log
import os, sys, re
import string
from pprint import pprint

sys.path.append('../')
from word2keypress import Keyboard
import random, string, json
from collections import defaultdict
from config import ALLOWED_KEYS, BLANK, dp, ENDSTR, STARTSTR, \
    what_case, ALLOWED_CHARACTERS, CASE_TITLE, CASE_UPPER

KB = Keyboard('qwerty')
MAX_ALLOWED_EDITS = 3
UNWEIGHTED = False
DEBUG = False
DEBUG_1 = True


def confusions(w, s):
    confusing_sets = [set(['1', 'l', 'I']),
                      set(['o', '0', 'O'])]
    for cset in confusing_sets:
        if w in cset and s in cset:
            return True
    return False


WEIGHT_MATRIX = {(s, d): 1 if s != d else 0
                 for s in ALLOWED_KEYS
                 for d in ALLOWED_KEYS}


def fill_weight_matrix():
    global WEIGHT_MATRIX
    for k in WEIGHT_MATRIX:
        s, d = k
        WEIGHT_MATRIX[k] = 0 if s == d else \
            1.1 if KB.is_keyboard_close(s, d) \
                else 2.1


def update_weight_matrix(weight_matrix):
    global WEIGHT_MATRIX
    for rc in ALLOWED_KEYS:
        total = sum(weight_matrix.get((rc, x), 0.0)+WEIGHT_MATRIX[(rc, x)]
                    for x in ALLOWED_KEYS)
        for tc in ALLOWED_CHARACTERS:
            WEIGHT_MATRIX[(rc, tc)] = total/weight_matrix[(rc, tc)]


def _insert(c, pos=0):
    if UNWEIGHTED:
        return 1
    global WEIGHT_MATRIX
    l = (BLANK, c)
    return WEIGHT_MATRIX.get(l, 1.0)
    # cost = 2.1 if pos < 1 else 1.1
    # if WEIGHT_MATRIX[l]<0:
    #    WEIGHT_MATRIX[l] = cost
    # return cost


def _delete(c, pos=2):
    if UNWEIGHTED:
        return 1
    global WEIGHT_MATRIX
    l = (c, BLANK)
    return WEIGHT_MATRIX.get(l, 1.0)
    # cost = 2.1 if pos < 1 else 1.1
    # if WEIGHT_MATRIX[l]<0:
    #    WEIGHT_MATRIX[l] = cost 
    # dp(_func='_delete', pos=pos, c=cost)
    # return cost


def _nothing(c, d, pos=0):
    extra_weight = 0.0 # (pos + 1) / 1000.0  # just to bias match in the beginning.
    return (4 if c != d and not confusions(c, d)
            else 0) + extra_weight


def _replace(c, d):
    if UNWEIGHTED:
        return 1
    global WEIGHT_MATRIX
    l = (c, d)
    epsilon = 0.0
    if l not in WEIGHT_MATRIX:
        return 3.0
    if WEIGHT_MATRIX[l] < 0:
        WEIGHT_MATRIX[l] = min(KB.keyboard_dist(c, d), 3)
    # print "WEIGHT MATRIX:", l, WEIGHT_MATRIX[l]
    return WEIGHT_MATRIX[l] + epsilon


def weditdist(s1, s2):
    """
    weighted edit distance to convert s1 to s2. So basically, 
    what I have to insert/delete/replace to *CREATE s2 FROM s1*
     \s2
    s1\-------------------
      | (i-1,j-1)   (i-1,j)
      | (i  ,j-1)   (i  ,j)
    """
    n1, n2 = len(s1) + 1, len(s2) + 1
    A = [[0 for j in range(n2)] for i in range(n1)]

    A[0][0] = (0, '')
    for i in range(1, n1):
        A[i][0] = (A[i - 1][0][0] + _delete(s1[i - 1], pos=i - 1), 'd')
    for j in range(1, n2):
        A[0][j] = (A[0][j - 1][0] + _insert(s2[j - 1], pos=j - 1), 'i')

    for i in range(1, n1):
        for j in range(1, n2):
            A[i][j] = min((A[i - 1][j][0] + _delete(s1[i - 1], pos=i - 1), 'd'),
                          (A[i][j - 1][0] + _insert(s2[j - 1], pos=j - 1), 'i'),
                          (A[i - 1][j - 1][0] + _replace(s1[i - 1], s2[j - 1]), 'r'),
                          (A[i - 1][j - 1][0] + _nothing(s1[i - 1], s2[j - 1], max(i, j)), ''),
                          )

    if DEBUG:
        dp(n1=n1, n2=n2, i=i, j=j)
        print '\n'.join(str(a) for a in A)
    edits = []
    # i, j = n1-1, n2-1  #i,j is already in the correct value
    w, _ = A[i][j]
    while i >= 0 and j >= 0:
        if DEBUG:
            dp(Aij=A[i][j])
        _, e = A[i][j]
        if e:
            edits.append((i - 1, j - 1, e))
        if e == 'd':
            i -= 1
        elif e == 'i':
            j -= 1
        else:
            i, j = i - 1, j - 1
    # print w, edits
    return w, edits[::-1]


def align(s1, s2, try_breaking_replace=False):
    """
    Aligns s1 and s2 according to the best alignment it can think of.
    :param s1: a basestring object
    :param s2: a basestring object
    :param try_breaking_replace: boolean, denoting whether or not to consider
    replace as a delete and insert
    :return: pair of strings of equal length, with added blanks for insert and delte.
    """
    assert isinstance(s1, basestring) and isinstance(s2, basestring), \
        "The input to align should be two basestrings only it will apply the keypress" \
        "function by itself."

    # print "{!r} <--> {!r}".format(s1, s2)
    s1k = KB.word_to_keyseq(s1)
    s2k = KB.word_to_keyseq(s2)

    w, edits = weditdist(s1k, s2k)
    if DEBUG:
        print "Edit distance:", w, edits
    inserted_in = [1, 1]
    s1klist = [STARTSTR] + list(s1k) + [ENDSTR]
    s2klist = [STARTSTR] + list(s2k) + [ENDSTR]
    if DEBUG:
        print w, edits
    for i1, i2, e in edits:
        #  print i1, i2, e
        if e == 'd':
            s2klist.insert(inserted_in[1] + i2+1, BLANK)
            inserted_in[1] += 1
        elif e == 'i':
            s1klist.insert(inserted_in[0] + i1+1, BLANK)
            inserted_in[0] += 1
        if try_breaking_replace and e == 'r':
            s2klist.insert(inserted_in[1] + i2, BLANK)
            s1klist.insert(inserted_in[0] + i1, BLANK)
            inserted_in[0] += 1
            inserted_in[1] += 1

    return ''.join(s1klist), ''.join(s2klist)


def is_series_insertion(s, t):
    #  s,t = ''.join(s), ''.join(t)
    regex = '^(?P<front>{0}{{2,}})|(?P<back>{0}{{2,}})$'.format(re.escape(BLANK))
    m = re.search(r'%s' % regex, s)
    if m:
        front, back = m.groups(['front', 'back'])
        t_priv = t[m.start():m.end()]
        if front:
            s_ = s[m.end():]
            t_ = t[m.end():]
        elif back:
            s_ = s[:m.start()]
            t_ = t[:m.start()]

        return s_, t_, t_priv
    return s, t, None


def extract_edits(w, s, N=1):
    """
    It extracts the edits between two strings @w and @s.
    @N denotes the amount of history and future that will
    be used while considering edits. E.g.,
    @w = 'raman', @s = 'Ramn'
    First will align the texts, 'raman' <--> 'Ram*n'
    Edits:
        r->R, ^r -> ^R, ^ra -> ^Ra, a->*, ma -> m, man-> m*n
        an -> *n
    @returns: an array of edit tuple, e.g. [('r', 'R'), ('^r', '^R')..] etc.
    NOTE: ^ and $ are used to denote start and end of a string.
    In code we use "\1" and "\2".
    """
    assert len(w) == len(s), \
        "Length of w (%s - %d) and s (%s - %d) are not equal." % (w, len(w), s, len(s))
    assert N >= 0, "N must be >= 0"
    if w == s:  # ******* REMEMBER THIS CHECK **********
        return []  # No edit if strings are equal

    c_w, c_s = what_case(w), what_case(s)
    E = []
    if w.lower() == s.lower():  # if differ by only case
        # Just differ by case
        return [(c_w, c_s)]
    # if case differ completely for w and s
    elif c_w != c_s and c_w != CASE_TITLE and c_s != CASE_TITLE:
        E.append((c_w, c_s))
        s = s.upper() if c_w is CASE_UPPER \
            else s.lower()

    w = STARTSTR + w + ENDSTR
    s = STARTSTR + s + ENDSTR
    e_count = 0
    for i, t in enumerate(zip(w, s)):
        c, d = t
        if c != d and not confusions(c, d):
            e_count += 1
            # start anywhere between i-N to i
            for j in xrange(-N, 1):
                # end anywhere between i to i+N
                for k in xrange(1, max(2, N + 1)):
                    E.append((w[i + j:i + k], s[i + j:i + k]))

    return E if e_count < 5 else []


def all_edits(orig, typed, N=1):
    if DEBUG:
        print "{0:*^30} --> {1:*^30}".format(orig, typed)
    s, t = align(orig, typed)
    # modify series insertion
    s, t, typed_priv = is_series_insertion(s, t)
    s_priv, t_priv = '', ''
    if DEBUG:
        print "Typed Extra:", typed_priv
    if typed_priv:
        s_priv, t_priv = align(orig, typed_priv)
        # remove_end_deletes
        s_priv, t_priv, _ = is_series_insertion(s_priv, t_priv)
        num_edits = len([1 for x, y in zip(s_priv, t_priv) if x != y and not confusions(x, y)])
        if num_edits > MAX_ALLOWED_EDITS:
            s_priv = ''
            t_priv = ''
    # print "%s\t\t%s\n%s\t\t%s" % (s_priv, s, t_priv, t)
    return extract_edits(s_priv, t_priv, N=N) + extract_edits(s, t, N=N)


def count_edits(L):
    W_Matrix = defaultdict(list)
    for orig, typed in L:
        if DEBUG_1:
            print orig, typed,
        for c, d in all_edits(orig, typed, N=1):
            if DEBUG_1:
                print str((c, d)),
            # W_Matrix[c][d] = W_Matrix[c].get(d, []).append((orig, typed))
            W_Matrix[(c, d)].append((orig, typed))
        print
    # for x, d in W_Matrix.items():
    #     f = float(sum(d.values()))
    #     for k in d:
    #         d[k] /= f

    return W_Matrix


import unittest


class TestWeditDistance(unittest.TestCase):
    def test_weditdistance_basic(self):
        global UNWEIGHTED
        old_uw = UNWEIGHTED
        UNWEIGHTED = False
        inp_res_map = [(('', ''), (0.0, [])),
                       (('', 'a'), (2.5, [(-1, 0, 'i')])),
                       (('a', 'a'), (0.0, [])),
                       (('a', 'b'), (3.0, [(0, 0, 'r')])),
                       (('aaa', 'aa'), (2.0, [(1, 0, 'd')])),
                       (('a', 'aa'), (1.5, [(0, 1, 'i')])),
                       (('abcd', 'a'), (6.0, [(1, 0, 'd'), (2, 0, 'd'), (3, 0, 'd')])),
                       (('abcd', 'Abc'), (2.2, [(0, 0, 'r'), (3, 2, 'd')])),
                       ]

        for inp, res in inp_res_map:
            res_ = weditdist(*inp)
            self.assertEqual(res_, res, "%s --> %s {{%s}}" % (inp, res, res_))
        UNWEIGHTED = old_uw

    def test_replace(self):
        global UNWEIGHTED
        old_uw = UNWEIGHTED
        UNWEIGHTED = False
        inp_res_map = [(('a', 'a'), (0)),
                       (('a', 'A'), (0.2))
                       ]
        for inp, res in inp_res_map:
            self.assertEqual(_replace(*inp), res)  # , "%s --> %s" %(str(inp),res))
        UNWEIGHTED = old_uw

    def test_aligned_text(self):
        inp_res_map = [(('', 'a'), (BLANK, 'a')),
                       (('a', ''), ('a', BLANK)),
                       (('abcd', 'ABCD'), ('abcd', 'ABCD')),
                       (('aAaA', 'AaA'), ('aAaA', '\0AaA')),
                       ]

        for n in xrange(6):
            a = list(ALLOWED_CHARACTERS[:-5])
            a.remove(BLANK)
            random.shuffle(a)
            t = a[:n + 1]
            s = t[:]
            for i in xrange(random.randint(1, n + 1)):  # how many edit
                loc = random.randint(0, len(s))
                if random.randint(0, 1) == 0:  # what edit -- 0: insert
                    s.insert(loc, a.pop())
                    t.insert(loc, BLANK)
                else:  # 1: delete
                    t.insert(loc, a.pop())
                    s.insert(loc, BLANK)
            s, t = ''.join(s), ''.join(t)
            inp_res_map.append(((s.replace(BLANK, ''),
                                 t.replace(BLANK, ''), True), (s, t)))
        for inp, res in inp_res_map:
            try:
                self.assertEqual(align(*inp), res)
            except:
                if len(res[1]) > 5:
                    continue
                else:
                    print align(*inp)
                    print res


if __name__ == '__main__':
    # fill_weight_matrix()
    # unittest.main()
    w1 = 'tryagain'
    w2 = 'ryagain'
    print '\n'.join(''.join(s) for s in align(w1, w2))
    # w, s = aligned_text(KB.key_presses('GARFIELD'), KB.key_presses('garfied'))
    # print w
    # print s
    # print all_edits(w,s)
    exit(0)
    L = []
    for k, v in json.load(open(sys.argv[1])).items():
        for x in v:
            if k != x[1]:
                L.append((k, x[1]))
    # print '\n'.join(str(x) for x in L)
    # E = {str((k,x)): v[x] for k,v in count_edits(L).items()
    #     for x in v if x.lower()!=k.lower()}
    E = {k: (len(v), v) for k, v in count_edits(L).items()}
    E = sorted(E.items(), key=lambda x: x[1][0], reverse=True)
    print sum(v[0] for k, v in E)
    print pprint(E)
    # print '\n'.join("%s|%s, %s" % (k[0], k[1], v[0]) for k,v in E)

import os
import random
import sys
from Levenshtein import distance as ed
import itertools 
import json, gzip
from keyboard import KeyPresses, Keyboard

T_RND = 9
REPEAT_N = 4

def hamming(x,y):
    """
    # @x and @y are binary strings.
    # @returns an integer which is the hamming distance between x and y
    >>> hamming('0', '1')
    1
    >>> hamming('00', '00')
    0
    >>> hamming('0101', '1101')
    1
    >>> hamming('0101', '1010')
    4
    """
    return sum(1 if xi!=yi else 0
               for xi, yi in zip(x,y))


def rand_hash(r, b, c):
    """
    # @r: random binary string, @b: 0 or 1, @c: an ascii character
    # r must of 8-bit long
    >>> rand_hash('10010100', 1, 'a') == ('{:b}'.format(int('10010100')&ord('a')).count('1') + 1) % 2
    True
    """
    binaryc = '{:08b}'.format(ord(c))
    return (sum(1 if xi==ri=='1' else 0
               for xi, ri in zip(binaryc,r)) + b) % 2


def embed(x, r):
    """
    # @x is a ascii string, @r is a random numeber of size 9*|x|
    # @returns an embedding of x under r. Following the construction
    # given in paper: http://iuuk.mff.cuni.cz/~koucky/papers/editDistance.pdf
    >>> embed('a', '010010011001'*4)
    'a000'
    """
    n = len(x)
    i = 0
    ret = ''
    for j in xrange(REPEAT_N * n):
        rj, b = r[j*T_RND:j*T_RND+8], int(r[j*T_RND+8])
        ret += x[i] if i<len(x) else '0'
        i += rand_hash(rj, b, x[i] if i<len(x) else '0')
    return ret


def c_shingles(w, c=3, allc=False):
    """
    # @w a string, @c an positive integer. 
    # Creates c-shinges of the string w.  
    # if @allc is True, then output all the c-shingles till @c. 
    # @returns: a list containg all the c_shingles
    >>> c_shingles('abcd', c=2, allc=False)
    ['ab', 'bc', 'cd']
    >>> c_shingles('abcd', c=2, allc=True)
    ['a', 'b', 'c', 'd', 'ab', 'bc', 'cd']
    """
    if allc:
        return list(itertools.chain(*(c_shingles(w, ci, allc=False) for ci in xrange(1, c+1))))
    else:
        return [w[i:i+c]
                for i in xrange(len(w)-c+1)]

def setdiff_distance(a, b):
    """
    # @a and @b are sets. 
    # This is almost like set difference, except,
    # there can be repeated elements. Two elements are equal if their
    # values are equal and their position in the arrays are close
    # (absolute diff of the positions is less than 1).

    >>> setdiff_distance(c_shingles('abcde'), c_shingles('bcdef'))
    ['abc', 'def']
    """
    n = len(set(a) ^ set(b))
    return float(n)/(len(set(a)) + len(set(b)))


def mydistance_set(x, y):
    """My custom built distance metrics that is built on top of set
    difference. 
    NOTE: Experimenting with this funciton.
    @x and @y are two stirngs. @returns a float stating the distance between two strings
    """
    if len(x)< len(y):
        x += '0'*(len(y)-len(x))
    if len(y)< len(x):
        y += '0'*(len(x)-len(y))

    assert len(x) == len(y)
    nr = len(x) * REPEAT_N * T_RND
    r = ('{:0%db}' % (nr)).format(random.getrandbits(nr))
    xe = embed(x, r)
    ye = embed(y, r)
    csx = c_shingles(x, c=3) 
    csy = c_shingles(y, c=3) 
    return setdiff_distance(csx, csy)

def test_setdiffdistance():
    """Open the database/pwresults.json.gz file and test the erroneous
    typos and try to argue if the set difference metric is okay
    """
    KB = Keyboard('US')
    for row in json.load(gzip.open('../database/pwresults.json.gz')):
        rpw, tpw = row['rpw'], row['tpw']
        rpw = str(KeyPresses(KB, rpw.strip()))
        tpw = str(KeyPresses(KB, tpw.strip()))
        if rpw != tpw:
            print "{}, {}, {}, {:4.2f}".format(rpw, tpw, ed(rpw, tpw), mydistance_set(rpw, tpw))


if __name__ == '__main__':
    test_setdiffdistance()
    exit(0)
    x = 'passwordsa'
    y = 'pasaswords'

    if len(x)< len(y):
        x += '0'*(len(y)-len(x))
    if len(y)< len(x):
        y += '0'*(len(x)-len(y))

    assert len(x) == len(y)
    nr = len(x) * REPEAT_N * T_RND
    r = ('{:0%db}' % (nr)).format(random.getrandbits(nr))
    xe = embed(x, r)
    ye = embed(y, r)
    print "Edist({}, {}) = {}".format(x,y, ed(x,y))
    print "Hdist({}, {}) = {}".format(xe,ye, hamming(x,y))
    
    print "#"*80
    print "C-Shingles"
    csx = c_shingles(x, c=3) 
    csy = c_shingles(y, c=3) 
    print "setdiff({}, {}): {}".format(csx, csy, setdiff_distance(csx, csy))
    csxe = c_shingles(xe, c=3, allc=True) 
    csye = c_shingles(ye, c=3, allc=True) 
    xeyediff = mydistance_set(x, y)
    print "setdiff({}, {}): {}".format(xe, ye, xeyediff)
    

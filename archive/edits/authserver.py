__author__ ='Rahul Chatterjee'

import os, sys, json, csv
import string, re
import unittest, string
from collections import defaultdict
from edits import Edits
import heapq
import helper 
from config import *


home_dir = os.path.expanduser("~")
PW_FILTER = lambda x: helper.is_asciistring(x) and len(x)>=6

def get_qth_most_prob_pw(pwmodel, q):
    if not pwmodel or q<=0:
        return (1e6,'')
    heap = [(0,'') for _ in range(q)]
    heapq.heapify(heap)
    for k,f in pwmodel:
        if heap[0][0] < f:
            heapq.heapreplace(heap, (f, k))
    print '\n'.join(str(r) for r in heap)
    return heap[0]


class AuthServer(object):
    transform_list = []
    transform_list_prob = {}

    #BLACK_LIST = set(x.strip() for x in open("%s/passwords/banned_list_ry1k" % home_dir))
    BLACK_LIST = set(x.strip() for x in open("%s/passwords/banned_list_twt" % home_dir))
    def __init__(self, _transform_list, policy_num=1, pwmodel=None):
        self.transform_list = _transform_list
        if 'same' not in self.transform_list:
            self.transform_list = ['same'] + self.transform_list
        self.edits = Edits()
        self.policy_num = policy_num
        self.check = eval("self.policy%d" % policy_num)
        # if pwmodel:
        #     # assert pwmodel.is_eval_model(), "Only accepts 'evaluation_models' of 'rockyou'."\
        #     #     "Got {}".format(pwmodel)
        self.pwmodel = pwmodel 

        q = 1000  # Fix the q
        self._q = q
        if q in [1,100,1000]:
            self.rpw_q = {
                1: (0.009327963973715412, u'123456'),
                100: (0.00018750321248445416, u'miguel'),
                1000: (4.042677065886588e-05, u'morena')
            }[q][0]
        else:
            self.rpw_q = get_qth_most_prob_pw(self.pwmodel, q)[0]
        self.setup_typo_probs()

    def setup_typo_probs(self):
        tmp_d = {t: TYPO_FIX_PROB[t] for t in self.transform_list}
        total = float(sum(tmp_d.values()))
        self.transform_list_prob = {t: tmp_d[t]/total
                                       for t in self.transform_list}

    def get_ball(self, tpw):
        return self.check(tpw)

    def get_ball_union(self, tpwlist):
        B = set()
        for tpw in tpwlist:
            B |= self.get_ball(tpw)
        return B

    @property
    def max_ball_size(self):
        return len(self.transform_list)

    @property
    def max_nh_size(self):
        return 10*len(self.transform_list)
    
    def get_nh(self, rpw):
        return self.edits.fast_modify(rpw, self.transform_list,
                                      typo=True, pw_filter=PW_FILTER)
        

    def set_approx_pwmodel(self, q):
        """Approx pw model is we divide the pw dist into 3 steps, first high
        probable zone, then mid prob zone and finally low probablity
        zone.  For each zone there is a defacto probability is set,
        and any pw falling in that zone receives that prob
        """
        pass

    def set_pwmodel(self, pwm):
        self.pwmodel = pwm

    def __str__(self):
        tl1, tl2 = self.transform_list[0], self.transform_list[-1]
        tlstr = '[' + '...'.join([str(tl1), str(tl2)]) + ']'
        return 'AuthServer: transforms=({}) {} (policy={}) (pwmodel={})'\
            .format(len(self.transform_list), tlstr, self.policy_num, self.pwmodel)

    def policy1(self, tpw, rpw=None):
        """This policy is just breat the ball around tpw using given the
        transforms and checks whether or not rpw in that ball
        """
        B = self.edits.fast_modify(tpw, apply_edits=self.transform_list)
        if rpw:
            return rpw in B
        else: 
            return B
    
    def policy2(self, tpw, rpw=None):
        """
        Dont allow any password within the blacklisted set of passwords
        """
        black_list_filter = lambda x: (len(x)>=6) and (x.lower() not in AuthServer.BLACK_LIST)
        B = self.edits.fast_modify(tpw, apply_edits=self.transform_list, pw_filter=black_list_filter)
        B.add(tpw)
        if rpw:
            return rpw in B
        else:
            return B

    def policy3(self, tpw, rpw=None):
        """
        Allow at most one password from the black list
        """
        black_list = []
        B = []
        for ename in self.transform_list:
            transform, typo = self.edits.EDITS_NAME_FUNC_MAP[ename]
            rrpw = transform(tpw)
            if not rrpw or len(rrpw)<6: continue
            if isinstance(rrpw, basestring):
                rrpw = [rrpw]
            for t in rrpw:
                if t.lower() in AuthServer.BLACK_LIST:
                    black_list.append(t)
                else:
                    B.append(t)
        if len(black_list)>0:
            B.append(black_list[0])
        B = set(B)
        if rpw:
            return rpw in B
        else:
            return B

    def policy4(self, tpw, rpw=None):
        """
        Don't correct a tpw if the size of the ball is bigger than
        rpw_q, requires exact pwmodel
        """
        if rpw and tpw==rpw:
            return True
        B = self.edits.fast_modify(tpw, apply_edits=self.transform_list)
        B.add(tpw)
        ballmass = sum(self.pwmodel.get(tpw) for tpw in B)
        if ballmass > self.rpw_q:   # Sorry no typo correction 
            # print "Sorry no typo corr for: {} -> {}".format(tpw, B)
            B = set([tpw])
        if rpw:
            return rpw in B
        else:
            return B

    def policy5(self, tpw, rpw=None):
        """
        Same as policy 4, but approximate info about pwmodel
        """
        black_list = []
        A = defaultdict(int)
        for ename in self.transform_list:
            transform, typo = self.edits.EDITS_NAME_FUNC_MAP[ename]
            rrpw = transform(tpw)
            if not rrpw or len(rrpw)<6: continue
            w = self.pwmodel.get(rrpw)*self.transform_list_prob.get(ename, 0.0)
            A[rrpw] += w
        B = set()
        B.add(tpw)
        if tpw in A:
            del A[tpw]

        B |= set(get_most_val_under_prob(A, self.pwmodel,
                                         self.rpw_q - self.pwmodel.get(tpw)))
        if rpw:
            return rpw in B
        else:
            return B

    def typo_fix_rate(self):
        from database.dbaccess import get_typos, total_correct
        total_num_typo = 0
        fixed = 0
        for row in get_typos(popular_only=True):
            if row['istypo']:
                total_num_typo += 1
                rpw, tpw = row['rpw'], row['tpw']
                if self.check(tpw, rpw):
                    fixed += 1
        return fixed, total_num_typo, total_correct()

class AuthServerRandomized(AuthServer):
    """This is Randomized version of the AuthServer where, the server
    stores hashes of a random subset of typos of a real password and
    hope that the entered password is one of them. @little_k is the
    number of hashes of the typos that is stored.
    """
    from helper import random
    def __init__(self, _transform_list, policy_num=1, little_k=1,
                 pwmodel=None):
        super(AuthServerRandomized, self).__init__(_transform_list, 
                                                   policy_num=policy_num,
                                                   pwmodel=pwmodel)
        self.little_k = little_k
    
    def check_r(self, tpw, rpw=None):
        B = self.check(tpw)
        rB = random.sample(B, min(self.little_k, len(B)), unique=True)
        if rpw:
            return rpw in rB
        else:
            return rB

    def typo_fix_rate(self):
        from database.dbaccess import get_typos, total_correct
        import numpy as np
        total_num_typo = 0
        fixed = []
        for row in get_typos(popular_only=True):
            if row['istypo']:
                total_num_typo += 1
                rpw, tpw = row['rpw'], row['tpw']
                if self.check(tpw, rpw):
                    fixed.append(min(1.0, self.little_k/float(len(self.get_nh(rpw)))))
                else:
                    print rpw, tpw
        mean = sum(fixed)/total_num_typo
        std =  sum(f*(1-f) for f in fixed)/total_num_typo
        print "Num fixed:", len(fixed)
        print "Total typo:", total_num_typo
        print "fixed: mean={}, std={}".format(mean, std)
        print "Total correct:", total_correct()
        return mean, std, total_num_typo, total_correct()


def get_most_val_under_prob(A, probs, p_cutoff):
    """Brute force find optimal subset of A, such that total weight of B
    is less than p_cutoff

    """
    assert len(A)<10, "Not going to work for large sets, len(A)={}".format(A)
    try:
        return max(filter(lambda x: sum(probs.get(a) for a in x)<=p_cutoff, helper.getallgroups(A.keys())),
               key=lambda x: sum(A[y] for y in x))
    except ValueError:
        return []

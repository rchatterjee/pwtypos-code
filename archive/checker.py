# from security.config import *
# from edits.edits import *
# import json, os, sys, csv
#
# pol1 = AuthServer(ALLOWED_EDITS, 1)
#
# def get_all_info(fname):
#     """
#     Takes a guesslist file name, and checks whatever you want
#     """
#     D = json.load(open(fname))
#     g1 = D['1_same']['Guesses']
#     g2 = D['2_swc-all']['Guesses']
#     return g1, g2
#
#
# ### CREATE all useful objects all here
# from security.pwmodel import PWModel
# from edits.authserver import AuthServer
#
# ## Correctors
# top5 = ['same', 'swc-all', 'swc-first','rm-lastc', 'rm-firstc', 'n2s-last']
# top3 = ['same', 'swc-all', 'swc-first','rm-lastc']
# top2 = ['same', 'swc-all', 'swc-first']
#
# ## PWmodels
# pwmr_eval = PWModel(leakname='rockyou', evaluation_model=True)
# pwmr = PWModel(leakname='rockyou', evaluation_model=False)
# pwmm = PWModel(leakname='myspace', evaluation_model=False)
# pwmp = PWModel(leakname='phpbb', evaluation_model=False)
#
# ## Exact checker
# chkexact = AuthServer(['same'], policy_num=1, pwmodel=pwmr_eval)
#
# ## Cose to optimal checker
# chkC_top3 = AuthServer(top3, policy_num=5, pwmodel=pwmr_eval)
# chkC_top5 = AuthServer(top5, policy_num=5, pwmodel=pwmr_eval)
#
# ## Checker with blacklist
# chkB_top3 = AuthServer(top3, policy_num=2, pwmodel=pwmr_eval)
# chkB_top5 = AuthServer(top5, policy_num=2, pwmodel=pwmr_eval)
#
# ## Checker with no restriction
# chkA_top3 = AuthServer(top3, policy_num=1, pwmodel=pwmr_eval)
# chkA_top5 = AuthServer(top5, policy_num=1, pwmodel=pwmr_eval)

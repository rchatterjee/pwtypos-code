import os, sys, re
import csv, json

fname = os.path.dirname(os.path.abspath(__file__)) + '/Oakland12passwords.tsv'
#fname = os.path.dirname(os.path.abspath(__file__)) + '/CHI14longpasswords.tsv'
print fname
def read_cmu_data(fname):
    with open(fname) as csvf:
        csvreader = csv.DictReader(csvf, delimiter='\t')
        def process(row):
            useful_fields = ['condition',
                            'password',
                            'creation_num_attempts',
                            'creation_attempts',
                            'did_store',
                            'time_forgot_password',
                            'recall1_num_attempts',
                            'recall1_attempts',
                            'lapse_between_days',
                            'recall2_num_attempts',
                            'recall2_attempts']
            for k in set(row.keys()) - set(useful_fields):
                del row[k]
            for k in useful_fields: # ['creation_attempts', 'recall1_attempts', 'recall2_attempts']:
                if k in row and row[k] and len(row[k])>0:
                    if (chr(2) in row[k]) or (chr(4) in row[k]) or re.match(r'(recall[12]|creation)_attempts', k):
                        row[k] = [w.strip() for w in re.split(r'[{}{}]+'.format(chr(2),chr(4)), row[k]) \
                                  if w.strip()]

                # else:
                #     row[k] =  [row[k]]
                # row[k] = filter(lambda x: x, row[k])
            # if len(row['recall1_attempts']) <=0 or len(row['recall2_attempts'])<=0:
            #     return False
            # rpw = row['password']
            # tpws2 = row['recall2_attempts']
            # if 'time_forgot_password' in row and row['time_forgot_password'] != 'false':
            #     l = row['recall2_attempts'].pop()
            #     if l.lower()!= rpw.lower(): # Probably they did not know that they should resubmit their passowrds. 
            #         row['recall2_attempts'].append(l)
            #     elif len(tpws2)==0:  # These are the lazy guys, just clicked the forget password link typed that. 
            #         return False
            # row['recall2_attempts']
            return True

        # json.dumps(row, indent=2, ensure_ascii=False)
        for row in csvreader:
            if process(row):
                yield row

def get_cmu_tpw_rpw(attempt=1):
    """
    Return the rpw, tpw from CMU data. Uses Oakland one for now. 
    """
    for row in read_cmu_data(fname):
        rpw = row['password']
        if attempt==1 or attempt==3:
            for tpw in row['recall1_attempts']:
                if tpw and len(tpw)>0:
                    yield rpw, tpw
        if attempt==2 or attempt==3:
            for tpw in row['recall2_attempts']:
                if tpw and len(tpw)>0:
                    yield rpw, tpw

def get_cmu_loginattempts(attempt=1):
    """Count the number of failed login attempts, that is how many user
    did not get the password correct, but one of their attempts were
    close.
    """
    for row in read_cmu_data(fname):
        rpw = row['password']
        tpws1 = row['recall1_attempts']
        tpws2 = row['recall2_attempts']
        if attempt==1:
            yield rpw, tpws1
        if attempt==2:
            yield rpw, tpws2

        if attempt==3:
            yield rpw, tpws1, tpws2, row['time_forgot_password'], row['lapse_between_days']


def failed_logins(attempt=1):
    for rpw, tpws in get_cmu_loginattempts(attempt=attempt):
        if tpws and rpw != tpws[-1]:
            yield rpw, tpws

def test_cmudata_random_things():
    for row in read_cmu_data(fname):
        if row['time_forgot_password'] != 'false':
            print row['password'], row['recall1_attempts'], row['time_forgot_password']



if __name__ == "__main__":
    # for t in get_cmu_loginattempts(attempt=3):
    #     print t
    # test_cmudata_random_things()
    import sys
    outfile = os.path.join('./', os.path.basename(sys.argv[1]).rsplit('.', 1)[0] + '.json')
    print outfile
    json.dump(list(read_cmu_data(sys.argv[1])), open(outfile, 'wb'), indent=2)

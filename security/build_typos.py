#!/usr/bin/env python3
"""
This code is used to generate the typos for a list of passwords. 

Papers to refer:
    1. https://www.cs.cornell.edu/~rahul/papers/pwtypos.pdf
    2. https://eprint.iacr.org/2017/810

There are several non-standard datastructure used in this file. 
See the `datastructure.md` file in the security folder for details.
"""

import os, sys, json
from word2keypress.typos import get_topk_typos

sys.path.insert(0, '/home/rahul/')
from passwords import readpw
import numpy as np
import marisa_trie
from gzip import GzipFile
from gzip import open as gzopen
from collections import defaultdict, Counter
from scipy import sparse
import pandas as pd
import random
from subprocess import Popen
from multiprocessing import Pool
import itertools
import time
import argparse

PW_TYPOS_DIST = None  # \tau_pw: distribution of typos for each password.
PREPROCESSED = False
ONLINE = False  # Whether to run for online attack or offline.
typos_trie = None
pws = None
RETURN_CACHE = False
TYPTOP_CACHE_SIZE = 5
NUM_SIMULATIONS = 500
NUM_SAMPLES = 200
N = -1
NUM_TYPOS = 1000


def get_typof_name(fname):
    return os.path.basename(fname).split('.', 1)[0] + '.typos'


def valid_typo(pw, typo):
    """Checks if a typo is allowed for a password. Employ three criteria that we
    had.  The typos are already only edit distance 1.  We just need to check the
    zxcvbn entropy the passwords. We use RockYou entropy of the typos.
    """
    p_pw = pws.prob(pw)
    p_typo = pws.prob(typo)
    return (p_typo < 0.0001) and (p_typo / p_pw < 8)


def _get_topk_typos(pw):
    try:
        return pw, [(typo, f) for typo, f in get_topk_typos(pw, NUM_TYPOS*2)
                    if typo != pw and valid_typo(pw, typo)][:NUM_TYPOS]
    except (ValueError, AssertionError) as e:
        print(e, "Ignoring:", pw)
        return []


def get_typos(pwlist):
    with Pool() as p:
        ret = itertools.chain(p.map(_get_topk_typos, pwlist))
    return ret


def sampler(typos_f, n):
    """Sample n typos from typos_f. typos_f is a list of floats"""
    a = np.asarray(typos_f)
    return np.random.choice(
        np.arange(len(typos_f)),
        size=n, replace=True, p=a / a.sum()
    )


# TODO: We don't simulate whole ChkPtxt, but this seems to be fine. Recheck
def simulate_LFU(t, transcript):
    return simulate_PLFU(t, transcript, lfu=True)


def simulate_MFU(t, transcript):
    return [w for w, f in Counter(transcript).most_common(t)]


def simulate_LRU(t, transcript):
    s = set()
    for i in transcript[::-1]:
        s.add(i)
        if len(s) == t:
            break
    return list(s)


def simulate_PLFU(t, transcript, lfu=False):
    cache = {}
    minj = -1
    for i in transcript:
        f = 0
        if i in cache:
            cache[i] += 1
            if i == minj: minj = -1
        elif len(cache) < t:
            cache[i] = 1
        else:  # cache is full, replace and insert
            if minj == -1:
                minj = min(cache, key=lambda x: cache[x])
            f = cache[minj]
            if lfu or random.randint(0, f + 1) > f:
                del cache[minj]
                cache[i] = (f + 1) if not lfu else 1
                minj = -1
    return list(cache.keys())


CACHES = ('LFU', 'PLFU', 'LRU', 'MFU')
typtop_FUNCS = (simulate_LFU, simulate_PLFU, simulate_LRU, simulate_MFU)


def simulate(typos_f, t, num_simulations, num_samples, return_cache=False):
    """This funciton simulate all the caches policies and compute the
    cache_inclusion_funciton for each typo. In 'freq_counter' variable, j-th row
    contains the probability that the j-th typo will be in the cache. i-th
    column represents the i-th cache policy.

    @typos_f: typos distribution, from typodist
    @t: the size of the cache
    @return_cache: If set true will return the cache instead of the typodist
    Returns the cached_typo_dist

    """
    assert np.all(typos_f[:-1] >= typos_f[1:]), (typos_f[:-1] - typos_f[1:])
    if return_cache:
        # The array is initialized with -1
        cache_arr = np.zeros((num_simulations, len(CACHES), t), dtype='int16') - 1
    else:
        freq_counter = np.zeros((len(typos_f), len(CACHES)))
    samples = sampler(typos_f, num_samples * num_simulations)
    for nsim in range(num_simulations):
        _t_samples = samples[nsim * num_samples:(nsim + 1) * num_samples]
        assert len(_t_samples) > 0
        for i, cache_func in enumerate(typtop_FUNCS):
            cache_entries = cache_func(t, _t_samples)
            assert len(cache_entries) <= t, (i, cache_func)
            if return_cache:
                cache_arr[nsim, i, :len(cache_entries)] = np.sort(cache_entries)
            else:
                freq_counter[cache_entries, i] += 1
    if return_cache:
        assert cache_arr.shape == (num_simulations, len(CACHES), t), \
            "{},\nshape={}".format(cache_arr, cache_arr.shape)
        return cache_arr
    else:
        return freq_counter / num_simulations


def get_id(typos_trie, typo):
    try:
        return typos_trie.key_id(typo)
    except (ValueError, KeyError) as e:
        print(e, "Ignoring typo:", typo)
        return -1


def compute_cached_typo_dist_for_w(l_i):
    """
    Returns typo_ids and their cached_typo_dist for each password / line.  l
    contains either the string and in that case it generates the passwords, else
    treat l as two arrays of same size, one containing the typo ids, and the
    other their probabilities.
    The first entry (l[0]) is the tuple (id, 0.) of the real password. The id is the same in pws
    variable.
    """
    if (l_i % 100 == 1):
        print("-->", l_i)
    l = PW_TYPOS_DIST[l_i]
    default_ret = (np.array([]), np.array([]))
    if not PREPROCESSED:
        if sys.version_info < (3, 6):
            l = l.decode('utf8')
        ldata = json.loads(l)
        if not ldata: return default_ret
        _pw, _typos = ldata
        typo_id_f_list = np.array([
            [get_id(typos_trie, w), f] for w, f in _typos
        ])
    else:
        if not np.any(l): return default_ret
    typo_id_f_list = l[l[1:, 1].argsort()[::-1] + 1]
    valid = typo_id_f_list[:, 0] >= 0
    typo_ids = typo_id_f_list[valid, 0].astype(int)
    typo_f = typo_id_f_list[valid, 1]
    cached_typo_dist = simulate(
        typo_f, t=TYPTOP_CACHE_SIZE,
        num_simulations=NUM_SIMULATIONS, num_samples=NUM_SAMPLES,
        return_cache=RETURN_CACHE
    )
    return typo_ids, cached_typo_dist


# def process_lines_parallel_gzip(typofname, line_st, line_end, epoch):
#     """
#     An approach to process the typofname in parallel and store the restuls into a
#     gzip file for further processing. Did not work out well. So, used
#     process_lines_parallel_npz.
#     """
#     global PW_TYPO_DIST, PREPROCESSED
#     PREPROCESSED = False
#     PW_TYPO_DIST = [
#         l
#         for lineno, l in enumerate(GzipFile(typofname + '.gz'))
#         if line_end >= lineno >= line_st
#     ]
#     p = Pool()
#     nproc = p._processes
#     args = [(s, s+epoch) for s in range(0, len(PW_TYPO_DIST), epoch)]
#     ret = p.map(process_lines, args)
#     p.close()
#     return itertools.chain(*ret)


def obtain_cached_typo_distribution(typofname):
    """
    Creates a typo.both.npz file containing the cached_typodist and typodist
    PW_TYPOS_DIST or typodist is a gigantic matrix,
       w~1 w~2 w~3 ...
    w1 0   3    2   1  0
    w2
    w3
    (i, j)-th entry in the matrix is cache-inclusion probability of j-th typo in the i-th password's cache.
    The matrix also has multiple layer, one each for cache policy.

    PW_TYPOS_DIST = n_pws arrays, each of different length.
    """

    global PW_TYPOS_DIST, PREPROCESSED, N
    PREPROCESSED = True
    if PW_TYPOS_DIST is None or len(PW_TYPOS_DIST) <= 0:
        PW_TYPOS_DIST = np.load(typofname + '.npz')['M']
    print("Done loading the file. Total #typos: {}".format(PW_TYPOS_DIST.shape))
    typo_bothdist_name = typofname + '.both.npz'
    if False and os.path.exists(typo_bothdist_name):
        cached_tyodist = np.load(typo_bothdist_name)['cachedtypodist']
        return ((a[1:, 0], a[1:, 1:]) for a in cached_tyodist)
    p = Pool()
    nproc = p._processes
    if N > 0:
        N = min(len(PW_TYPOS_DIST), N)
    else:
        N = len(PW_TYPOS_DIST)
    epoch = N // 16
    ret = p.map(compute_cached_typo_dist_for_w, range(N), chunksize=epoch)
    p.close()
    cached_typodist = np.array([None for _ in range(PW_TYPOS_DIST.shape[0])])
    print("Done running all the simulations")
    for i_pw, l in enumerate(ret):
        w_typo_ids, w_cached_typo_dist = l
        if not np.any(w_typo_ids): continue
        a = np.zeros((w_typo_ids.shape[0] + 1, len(CACHES) + 1))
        a[0, 0] = PW_TYPOS_DIST[i_pw][0, 0]  # 0-th location is the real password
        a[1:, 0] = w_typo_ids  # 1-onwards
        a[1:, 1:] = w_cached_typo_dist
        cached_typodist[i_pw] = a
        yield l
    np.savez_compressed(typo_bothdist_name,
                        typodist=PW_TYPOS_DIST,
                        cachedtypodist=cached_typodist)


def get_cache_sim_res(fname, cached_typodist=None):
    """This is newer and probably faster function.  Each row in cached_typodist is a
    (k, 5) matrix, where k is the number of typos of the password, first column
    in the matrix is the id of the typos, and next 4 columns are the 4 probs
    according to CACHE_POLICIES (the first row in each column is for the real
    password)

    """
    typofname = get_typof_name(fname)
    typo_bothdist_name = typofname + '.both.npz'
    assert os.path.exists(typo_bothdist_name), \
        "{} file need to exists, run the function process_lines_parallel_npz".format(
            typo_bothdist_name
        )
    if cached_typodist is None:
        cached_typodist = np.load(typo_bothdist_name)['cachedtypodist']
    typos = marisa_trie.Trie().load(typofname + '.trie')
    pws = readpw.Passwords(fname)
    # black list top 100 passwords as typo
    M = np.zeros((len(typos), len(CACHES)), dtype='f4')
    epoch = 1000

    blacklisted_typo_ids = np.array([
        1567097, 6632504, 1681363, 1265110, 17739695, 43080110,
        767048, 35337856, 43955979, 767049, 1157535, 6192871, 35013813,
        43762648, 32693999, 38183042, 1535592
    ], dtype=int)
    blacklisted_typo_ids = np.array([
        typos.key_id(pw) for _, pw, _ in pws.iterpws(1000)
    ])
    blacklisted_pw_ids = set([
        _id for _id, _, _ in pws.iterpws(1000)
    ])
    cache_id = 0
    maxweight_res = pd.DataFrame(columns=CACHES)
    for i, a in enumerate(cached_typodist):
        if not np.any(a): continue
        if a[0, 0] in blacklisted_pw_ids: continue
        M[a[1:, 0].astype(int)] += a[1:, 1:]
        if (i + 1) % epoch == 0:
            print("i={}".format(i))
            if i + 1 < len(cached_typodist): continue
            M[blacklisted_typo_ids] = 0
            maxweight_res.loc[i + 1] = [
                M[:, k].max() for k in range(len(CACHES))
            ]
            if maxweight_res.loc[i + 1][-1] > 4:
                _j = np.argmax(M[:, -1])
                while len(typos.restore_key(_j)) < 6 and M[_j, -1] > 4.0:
                    M[_j] = 0
                    _j = np.argmax(M[:, -1])
                    maxweight_res.loc[i + 1] = [
                        M[:, k].max() for k in range(len(CACHES))
                    ]
                    print("MFU shit: typoid: ", _j, typos.restore_key(_j), maxweight_res.loc[i + 1])
            maxweight_res.to_csv("res_maxweight.csv")


def _process_typo_file(typofname):
    n = 0
    print("Reading {} file and creating unique list of typos".format(typofname))
    typofname = typofname.rstrip('.gz')
    with open('/tmp/tt', 'w') as f:
        for l in gzopen(typofname + '.gz', 'rt'):
            if sys.version_info < (3, 5):
                l = l.decode('utf8')
            ldata = json.loads(l)
            if not ldata: continue
            n += 1
            f.write(ldata[0] + '\n')  # The real password first
            f.write('\n'.join(
                typo
                for typo, _ in ldata[1]
            ))
            f.write('\n')
    print("Finding uniq typos and then creating trie of the typos in the file: {}"
                  .format(typofname + '.trie'))
    p = Popen("sort -u /tmp/tt | ./trie.py  {}".format(typofname + '.trie'),
          shell=True)
    p.wait()
    print("Done creating the trie.")


def _create_typo_trie(fname, n=100000):
    """Creates trie of the typos from the file /tmp/tt.uniq containing 
    typos of the passwords in fname.
    M: Array[pw_i -> List of typos of pw_i]
    n = 100000 # n passwords are considered
    """
    typofname = get_typof_name(fname)
    # print(time.clock())
    # T = marisa_trie.Trie(
    #     l[:-1]
    #     for l in open('/tmp/tt.uniq')
    # )
    # T.save(typofname + '.trie')
    #
    # print(time.clock())
    # exit(0)
    # print("Saved trie file, will reread in 2 sec ...")

    print("Rereading the trie file...")
    T = marisa_trie.Trie().load(typofname + '.trie')
    # Create the mapping from password to typo
    print("Creating the mapping from typos to the passwords. Outfile: {}"
          .format(typofname + '.npz'))
    pw2tpws = []
    pws = readpw.Passwords(fname)
    for i, line in enumerate(gzopen(typofname + '.gz', 'rt')):
        if sys.version_info < (3, 5):
            line = line.decode('utf8')
        ldata = json.loads(line)
        if not ldata: continue
        pw, typos = ldata
        if i % 1000 == 0:
            print("Read from gzip file: {}".format(i))
        line = np.array(
            [[pws.pw2id(pw), 0.0]] + \
            [[T.key_id(typo), f] for typo, f in typos]
        )
        line[1:] = line[line[1:, 1].argsort()[::-1] + 1]
        pw2tpws.append(line)
    pw2tpws_arr = np.array(pw2tpws, dtype='O')
    np.savez_compressed(typofname + '.npz', M=pw2tpws_arr)


def create_typos(fname, num_pws=1000, random_sample=False, num_typos=1000):
    """Creates all the typos of @num_pws (default 1000) passwords from @fname. If
random_sample is true (default false), randomly sampled passwords will be used
instead of top-num_pws passwords. @num_typos is the number of typos generated
for each password.
    """
    global pws
    pws = readpw.Passwords(fname)
    if random_sample:
        topkpws = list(pws.sample_pws(num_pws, asperdist=False))
    else:
        topkpws = [pw for _id, pw, _ in pws.iterpws(num_pws)]

    outfname = get_typof_name(fname) + '.gz'
    print("Creating typos, and saving in {}".format(outfname))
    i = 0
    epoc = 10000
    with gzopen(outfname, 'wt') as outf:
        for i in range(0, num_pws, epoc):
            outf.write('\n'.join(
                json.dumps(x)
                for x in get_typos(topkpws[i:i + epoc])
                if x
            ))
            outf.write('\n')
            print("Createing Typos. Done {}".format(i))
    _process_typo_file(outfname)
    _create_typo_trie(fname, num_pws)


def read_typos_and_get_res(fname):
    """Reads the typo-files generated in the previous step, and run the greedy
    algorithm to find the maximum coverage.
    """
    global typos_trie, pws
    print("This tries to compute the edge weight (see CCS'17 paper by Chatterjee et al.)")
    print("Generating the guesses...")

    typofname = get_typof_name(fname)
    # pws = readpw.Passwords(fname)  # trie of real passwords
    typos_trie = marisa_trie.Trie().load(typofname + '.trie')
    maxweight_res = pd.DataFrame(columns=CACHES)
    # Keeps track of the weight of the ball around a typo
    # (whose id according to typos_trie trie is i)
    M = np.zeros((len(typos_trie), len(CACHES)))
    epoch = 1000  # epoch for printing out the max ball weight
    for i, t in enumerate(obtain_cached_typo_distribution(typofname)):
        typo_ids, cached_typo_dist = t
        if not np.any(typo_ids): continue
        M[typo_ids] += cached_typo_dist
        if i > 0 and i % epoch == 0:
            maxweight_res.loc[i] = [
                M[:, k].max() for k in range(len(CACHES))
            ]
            # To not loose data in the middle.
            maxweight_res.to_csv("res_maxweight.csv")
    maxweight_res.to_csv("res_maxweight.csv")


def argument_parser():
    parser = argparse.ArgumentParser(
        description="Generate attacker's advantage for online or offline attacks against typtop"
    )
    parser.add_argument('pwfile', help="the password file to work on",
                        action='store')
    parser.add_argument('--numpws', help="number of passwords to consider",
                        action='store', type=int)
    parser.add_argument('--attack',
                        help="type of attack: online/on or offline/off",
                        action='store', default='offline')
    parser.add_argument('--sample',
                        help="Take @numpws passwords at random or from top-k",
                        action="store", default="topk"
                        )
    parser.add_argument('--fresh', help="Ignore all the cached files, and start from scratch",
                        action="store_true", default=False)
    return parser


if __name__ == "__main__":
    myparser = argument_parser()
    args = myparser.parse_args()
    # if not args.online or not args.offline:
    #     parser.print_help()
    #     exit(-1)
    print(args)
    # create_typos(args.pwfile, num_pws=args.numpws,
    #              random_sample=args.sample == 'random')
    RETURN_CACHE = False
    read_typos_and_get_res(args.pwfile)
    # get_cache_sim_res(args.pwfile)

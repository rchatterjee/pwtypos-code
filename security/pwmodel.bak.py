#!/usr/bin/python

"""This file returns a model for passowrds.  It has basically two
apis, given a pw returns the probability, and an iterator that returns
(pws, prob) in decreasing order of the probability
"""

"""Currently therer are one option, use RockYou or some other website
leak.
"""
import sys, os
# sys.path.append('.')
import helper, json
from collections import OrderedDict, defaultdict
from hashlib import sha256 as sha2
import dawg
import heapq
import operator
import config

MIN_PROB = 1e-9
def fname2leakname(fname):
    return os.path.basename(fname).split('.')[0].split('-')[0]

class dawgWrapper():
    def __init__(self, fname, __filter):
        self._dawg = dawg.IntCompletionDAWG().load(fname)
        self.__filter = __filter

    def __iter__(self):
        for k,v in self._dawg.iteritems():
            if self.__filter(k):
                yield k, v

    def iteritems(self):
        return self.__iter__()

    def items(self):
        return self.__iter__()

    def get(self, k, r=0):
        if self.__filter(k):
            return self._dawg.get(k, r)
        return 0

    def keys(self):
        return self._dawg.iterkeys()


class PWModel(object):
    PW2FREQ_map = OrderedDict()
    supported = {"RY"}
    HARD_LIMIT = int(1e6)
    def __init__(self, fname=None, leakname=None, pw_filter=None, 
                 evaluation_model=False, limit=-1):
        # assert model in supported, "PWModel for '{}' not yet supported".format(model)
        self.evaluation_model = evaluation_model
        self.HARD_LIMIT = limit if limit>0 else config.LIMIT
        if not pw_filter:
            pw_filter = lambda x: len(x)>=6
        self.pw_filter = pw_filter
        self.read_pw_file(fname=fname, leakname=leakname)
        self.topq = {}

    def make_eval_model(self):
        self.evaluation_model = True
        self.PW2FREQ_map = self.PWdawg

    def read_pw_file(self, fname=None, leakname=None):
        assert leakname or fname, "Either provide leakname or the filename. Both are null now!!"
        if not leakname:
            leakname = fname2leakname(fname)
        self.leakname = leakname
        print "Leak name:", leakname, self.evaluation_model
        oldfl = 'data/data_{0}/all_{0}.dawg'.format(leakname)
        print "Checking the file: {}".format(oldfl)
        if os.path.exists(oldfl):
            print "File ({}) exists, so reading from there".format(oldfl)
        else:
            print "File ({}) does not exist, so creating! Hold on..".format(oldfl)
            pwmap = defaultdict()
            if not fname:
                print "Trying to guess the fname (currently: {!r})".format(fname)
                fname = os.path.expanduser('~/passwords/%s-withcount.txt.bz2' % leakname)
                print "Trying read the file: '{}'".format(fname)
                
            for pw, freq in helper.open_get_line(fname, limit=-1):
                if config.PW_FILTER(pw):
                    pwmap[unicode(pw)] = freq
            dirname = os.path.dirname(oldfl)
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            self.PWdawg = dawg.IntCompletionDAWG(pwmap.iteritems())
            self.PWdawg.save(oldfl)

        self.PWdawg = dawgWrapper(oldfl, self.pw_filter)
        if leakname=='rockyou':
            # ct, ft = 14332903, 32590665  -- this is whole file
            ct, ft = 14037485, 31167466  # only the passwords that pass pw_filter
        elif leakname=='weirpcfg':
            # ct, ft = 14332903, 32590665  -- this is whole file
            ct, ft = 9999948, 40975326  # only the passwords that pass pw_filter
        else:
            ct, ft = 0, 0
            for pw, f in self.PWdawg:
                ct += 1
                ft += f
        self._total_freq = float(ft)
        self._num_pw = ct
        print "Total Passwords: {}, total freq: {}".format(ct, ft)

        if not self.evaluation_model:
            print "Need: {} passwords".format(self.HARD_LIMIT)
            self.PW2FREQ_map = OrderedDict()
            for pw, f in heapq.nlargest(int(self.HARD_LIMIT*1.25), self.PWdawg, 
                                        key=operator.itemgetter(1)):
                if self.pw_filter(pw):
                    self.PW2FREQ_map[pw] = f
                if len(self.PW2FREQ_map)>self.HARD_LIMIT:
                    break
        else:
            self.PW2FREQ_map = self.PWdawg

        # seen_mass = sum(self.PW2FREQ_map.values())
        # assume the distribution is uniform after this and every thing in the tail has same probability
        # self._tail_prob = (self._total_freq - seen_mass)/self._total_freq
    
    def real_prob(self, pw):
        f = self.PWdawg.get(unicode(pw), 0)
        return max(f/self._total_freq, MIN_PROB)

    def rank(self, pwlist, guess_rank=True):
        """
        *** WRONG FUNCTION, TODO: Fix this ****
        returns the rank of pws in pwlist
        """
        
        if isinstance(self.PW2FREQ_map, OrderedDict):
            A = self.PW2FREQ_map.keys()
        elif isinstance(self.PW2FREQ_map, dawg.DAWG):
            A = [k for k,v in heapq.nlargest(self.HARD_LIMIT, self.PW2FREQ_map,
                               key=operator.itemgetter(1))]
        else:
            print "ERROR: PW2FREQ_map is of different type {}".format(type(self.PW2FREQ_map))
            return []

        ret = []
        for pw in pwlist:
            try:
                ret.append((A.index(pw)+1)*self.PW2FREQ_map[pw])
            except ValueError:
                ret.append(self.HARD_LIMIT*self.PW2FREQ_map[pw])
        return ret


    def sum_top_q(self, q):
        if len(self.topq)<q:
            if isinstance(self.PW2FREQ_map, dict):
                assert q<len(self.PW2FREQ_map), "value of q is too large"
                self.topq = OrderedDict(self.PW2FREQ_map.items()[:q])
            else:
                self.topq = OrderedDict(heapq.nlargest(q, self.PW2FREQ_map,
                                    key=operator.itemgetter(1)))
        return sum(self.topq.values()[:q])/self._total_freq


    def get(self, pw):
        return self.PW2FREQ_map.get(pw, 0.0)/self._total_freq

    def pw2frec(self, pw):
        return self.PW2FREQ_map.get(pw, 0.0)/self._total_freq

    def total_freq(self):                           
        return self._total_freq

    def __len__(self):
        if isinstance(self.PW2FREQ_map, dict):
            return len(self.PW2FREQ_map)
        else:
            return self._num_pw

    def __iter__(self):
        for pw, v in self.PW2FREQ_map.iteritems():
            yield pw, v/self._total_freq
    
    def __bool__(self):
        return True

    def __str__(self):
        return "PwModel: {} (lim={}, evalmodel={})".format(self.leakname, self.HARD_LIMIT,
                                                  self.evaluation_model)
    
    def is_eval_model(self):
        return self.evaluation_model


class ApproxPWModel(PWModel):
    def __init__(self, leakname, max_ball_size, q):
        super(PWModel, self).__init__()
        pass


def test_Pwmodel():
    limit = [100, 1000, 10000]
    for l in limit:
        pwm = PWModel(leakname='rockyou', limit=l)
        print 'Limit = {}, f_b={}'.format(l,  pwm.get('BEAUTIFUL'))
        
if __name__ == "__main__":
    assert fname2leakname('/home/rahul/passwords/rockyou-withcount.txt.bz2') == 'rockyou'
    assert fname2leakname('/home/rahul/passwords/myspace-withcount.txt.bz2') == 'myspace'
    test_Pwmodel()

#!/usr/bin/python
"""This file tries to calculate the fuzzy beta-success rate (l_b),
which is a adapted version of beta success rate mentioned by 
Bonneau. in Oakland 2012.

The possible typos accepted by the server blows up with number
of edits. To keep that in control, we only store top 1M, 
candidates. Downside it the calculation is accurate uptill
b = 10000 or 20000, guesses, but after that it might have tail cut off
error.
========= MATH ======== 
rpw = real password
tpw = typed password
tpw --> rpw --> ttpw
One rpw can generate BETA tpw's, on tpw can be accepted for GAMMA rpw's.

say, max_num_guesses = g then we shall never require, more than g *
BETA * GAMMA top candidates, in the array.

Now, 
"""

import operator, sys, json
import config
import helper
import heapq
from math import log
# from IPython.core import ultratb
# sys.excepthook = ultratb.FormattedTB(#mode='Verbose',
#      color_scheme='Linux', call_pdb=1)

LIMIT = config.LIMIT
TYPO_PW = {}
BETA = 0  # how many different passwords a typo can be mapped to.
GAMMA = 0  # maximum number of typos that will be mapped to a particular
LOCK_FREQ = 0
MAX_TYPO_LIST_SIZE = 0
killed_pw_set = set()
MAX_GUESS_ALLOWED = 10000

def reset_globals():
    global MAX_GUESS_ALLOWED, MAX_TYPO_LIST_SIZE, TYPO_PW
    global edits, LOCK_FREQ, BETA, GAMMA, killed_pw_set
    MAX_TYPO_LIST_SIZE = MAX_GUESS_ALLOWED
    TYPO_PW = dict()
    killed_pw_set = set()
    LOCK_FREQ = 0
    BETA = 0
    GAMMA = 0


# password
def add_to_typo_pw_list(aserver, pwm, tpwarr, c):
    global TYPO_PW, LOCK_FREQ
    if c<LOCK_FREQ and len(TYPO_PW)>MAX_GUESS_ALLOWED*BETA*GAMMA:
        if c>2e-6:
            print "Ignoring because frequecy ({}) is too low (cutoff: {}). Beta: {}, lenTYPO_PW={}"\
                .format(c, LOCK_FREQ, BETA, len(TYPO_PW))
        return
        
    for tpw in tpwarr:
        if tpw not in TYPO_PW:
            TYPO_PW[tpw] = sum(pwm.get(rpw) for rpw in aserver.check(tpw))
    if LOCK_FREQ == 0 and len(TYPO_PW)>MAX_GUESS_ALLOWED:
        helper.warning("Resetting LOCK_FREQ value", )
        LOCK_FREQ = c / (BETA+1)
        helper.warning("Lock Frequency: {}".format(LOCK_FREQ))
        helper.warning("BETA: {}, GAMMA: {}, MAX_GUESSES: {} TYPO_LIST size: {}" \
                       .format(BETA, GAMMA, MAX_GUESS_ALLOWED, len(TYPO_PW)))

    if len(TYPO_PW)>1.75*MAX_TYPO_LIST_SIZE:
        helper.warning("Shortening the TYPO_LIST value... currently={}, \tShouldbe={} f={}"\
                       .format(len(TYPO_PW), MAX_TYPO_LIST_SIZE, c))
        smallest = None
        b = [0, 0.0]
        for a in heapq.nsmallest(len(TYPO_PW)-MAX_TYPO_LIST_SIZE, 
                                 TYPO_PW.items(), 
                                 key=operator.itemgetter(1)):
            b = a[1]
            del TYPO_PW[a[0]]
        LOCK_FREQ = max(LOCK_FREQ, b/(BETA+1))
        helper.warning("Lock Frequency reset to: {}".format(LOCK_FREQ))
        helper.warning("Done! TYPO_LIST size={}".format(len(TYPO_PW)))
                       

def get_all_probable_typos(aserver, pwm):
    global BETA, GAMMA, MAX_TYPO_LIST_SIZE, LOCK_FREQ
    BETA = len(aserver.transform_list)
    GAMMA = BETA
    MAX_TYPO_LIST_SIZE = BETA * GAMMA * MAX_GUESS_ALLOWED
    helper.warning("MAX_TYPO_LIST_SIZE: {}".format(MAX_TYPO_LIST_SIZE))

    cnt = 0
    for rpw, c in pwm:
        if not cnt % 50000:
            helper.warning("cnt: {}, f: {}".format(cnt, c))
        if len(TYPO_PW)>MAX_TYPO_LIST_SIZE and c<LOCK_FREQ:
            break;
        if config.PW_FILTER(rpw):
            cnt += 1
            edited_tpws = aserver.get_nh(rpw)

            if GAMMA < len(edited_tpws):
                GAMMA = len(edited_tpws)
                MAX_TYPO_LIST_SIZE = BETA * GAMMA * MAX_GUESS_ALLOWED
                LOCK_FREQ = 0
                helper.warning("GAMMA updated to {}. Count: {}".format(GAMMA, cnt))
            add_to_typo_pw_list(aserver, pwm, edited_tpws, c)
    helper.warning("Finish reading the whole file")


def remove_what_covered(tpw, min_freq_cutoff, aserver, pwm):
    killed = 0
    global TYPO_PW, killed_pw_set
    rpwB = set(rpw for rpw in aserver.check(tpw) if rpw not in killed_pw_set and\
                         pwm.get(rpw)>0)
    kf = sum(pwm.get(rpw) for rpw in rpwB)
    if abs(kf-TYPO_PW[tpw])>1e-10:
        print "\nERROR: tpw_freq={} for tpw = {}, but it is killing only {},"\
            "\nwith total freq {}\n PWMODEL={},\nball(tpw) = {}"\
            "\nkilled_pw_set={}".format(TYPO_PW[tpw], tpw, rpwB, kf, pwm, 
                                        aserver.get_ball(tpw), killed_pw_set)
        print "typo_freq should be: ",  sum(pwm.get(rpw) for rpw in aserver.check(tpw))
        exit(0)

    tpwB = set()
    for rpw in rpwB:
        tpwB |= aserver.get_nh(rpw)

    killed_pw_set |= rpwB
    # print "Removing: ", tpw, "from ", tpwB, '\n', rpwB
    for ttpw in tpwB:
        for rpw in rpwB:
            if ttpw in TYPO_PW and aserver.check(ttpw, rpw):
                TYPO_PW[ttpw] -= pwm.get(rpw)
                if TYPO_PW[ttpw] <= min_freq_cutoff:
                    del TYPO_PW[ttpw]
                killed += 1
    if TYPO_PW.get(tpw, 0.0) > 0:
        print "Somethign wrong, '{}' (min_freq={}) ({}) should have"\
            "been deleted. Deleting now .. ".format(tpw, min_freq_cutoff, TYPO_PW.get(tpw))
        del TYPO_PW[tpw]
    return killed

def greedy_maximum_coverage_heap(name, num_guesses, aserver, pwm):
    """
    Same as greedy_maximum_coverage, except now it uses fib_heap
    """
    # import fibonacci_heap_mod as fibheap
    global TYPO_PW, MAX_GUESS_ALLOWED
    reset_globals()
    q = MAX_GUESS_ALLOWED = num_guesses
    BETA = len(aserver.transform_list)
    GAMMA = 42*BETA
    MAX_TYPO_LIST_SIZE = BETA * GAMMA * MAX_GUESS_ALLOWED

    # get_all_probable_typos(aserver, pwm)
    # helper.warning("{} ==> Size of the support of words {}".format(name, len(TYPO_PW)))
    pwm.sum_top_q(num_guesses)
    topq = pwm.topq.items()
   
    allowed_edits = aserver.transform_list
    guesses = []  # [(guess1, c1), (guess2, c2), ...]
    beta_gamma_estimate = 0
    shrink_counter = 0
    min_freq_cutoff = 0.0
    top1000 = pwm.PW2FREQ_map.keys()[:1000]
    total_win = 0.0
    from heap.heap import heap

    # Create the heap structure
    subset_heap = heap({})
    def setweight(tpw):
        return sum(pwm.get(rrpw) for rrpw in aserver.get_ball(tpw))

    print "Creating the fib-heap"
    
    for rpw, c in pwm:
        for tpw in aserver.get_nh(rpw):
            w = setweight(tpw)
            if w>0:
                subset_heap[tpw] = -w
        if len(subset_heap)>MAX_TYPO_LIST_SIZE:
            break
        
    print "Done creating the heap..."
    guess_list = []
    while len(guess_list)<q:
        [tpw, weight] = subset_heap.popitem()
        # print tpw, -weight
        guess_list.append((tpw, -weight))
        for rpw in aserver.get_ball(tpw):
            w = pwm.get(rpw)
            for tpw in aserver.get_nh(rpw):
                if tpw in subset_heap:
                    if subset_heap[tpw] >-w:
                        subset_heap.discard(tpw)
                    else:
                        subset_heap[tpw] += w
    return guess_list

def find_optimal_guesses(name, num_guesses, aserver, pwm):
    """
    return an array with @num_guesses top guesses with their
    success frequency (need to be normalized by total frequency 
    for probability).
    """
    global TYPO_PW, MAX_GUESS_ALLOWED
    reset_globals()
    MAX_GUESS_ALLOWED = num_guesses
    get_all_probable_typos(aserver, pwm)
    helper.warning("{} ==> Size of the support of words {}".format(name, len(TYPO_PW)))
    pwm.sum_top_q(num_guesses)
    topq = pwm.topq.items()
   
    allowed_edits = aserver.transform_list
    guesses = []  # [(guess1, c1), (guess2, c2), ...]
    beta_gamma_estimate = 0
    shrink_counter = 0
    min_freq_cutoff = 0.0
    top1000 = pwm.PW2FREQ_map.keys()[:1000]
    total_win = 0.0
    while len(guesses) < num_guesses:
        if len(TYPO_PW) < 1:
            print "WTH, TYPO_PW is empty. beta_gamma: {}, group: {},  done-guesses: {}, last-guess: {}" \
                .format(beta_gamma, len(allowed_edits), len(guesses), guesses[-1])
            break
        if min_freq_cutoff <= 0.0:
            TYPO_PW = dict(filter(lambda x: x[1]>0.0, TYPO_PW.items()))
            min_freq_cutoff = min(TYPO_PW.values())

        if shrink_counter <= 0:
            helper.warning("Beta_Gamma_Estimate: {}\t Num Guess left: {}"
                           .format(beta_gamma_estimate / (len(guesses) + 1), num_guesses-len(guesses)))
            beta_gamma = 2.0 * max(10, float(beta_gamma_estimate / len(guesses))) \
                         if beta_gamma_estimate > 0 else BETA * GAMMA  # random constant 10.0 ?!?
            t = int((num_guesses - len(guesses)) * beta_gamma) + 1
            if len(TYPO_PW)>t:
                for a in heapq.nsmallest(len(TYPO_PW)-t, TYPO_PW.items(), key=operator.itemgetter(1)):
                    TYPO_PW.pop(a[0])
                min_freq_cutoff = a[1]
            else:
                min_freq_cutoff = min(TYPO_PW.values())
            shrink_counter = int(10000 / log(len(TYPO_PW),2))
            helper.warning("Min Freq Cutoff: {}, Shrink_counter: {}, t: {}, BETA: {}, GAMMA: {}, " \
                           "beta_gamma: {}".format(min_freq_cutoff, shrink_counter, t,
                                                   BETA, GAMMA, beta_gamma))
        shrink_counter -= 1
        tpw, c = max(TYPO_PW.items(), key=lambda x: x[1])
        guesses.append((tpw, c))
        total_win += c
        #print tpw, c, aserver.get_ball(tpw), sum(pwm.get(pw) for pw in aserver.get_ball(tpw)), top1000[len(guesses)-1], pwm.get(top1000[len(guesses)-1])
        # print "Guess #{}: ({}, {}) --- \t\tSize: {} (DECEMBER: {})"\
        #    .format(len(guesses), tpw, c, len(TYPO_PW), pwm.get('DECEMBER'))
        # continue
        beta_gamma_estimate += remove_what_covered(tpw, min_freq_cutoff, aserver, pwm)
    print "killed accounts: {}".format(total_win)
    return zip(*guesses)[0]


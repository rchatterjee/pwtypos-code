import sys, os, math, re
from pwmodel import helper
from collections import defaultdict, OrderedDict
import operator, sys, json, operator
import multiprocessing, bisect, heapq
reload(sys)
sys.setdefaultencoding('utf-8')

PW_FILTER = lambda x: helper.is_asciistring(x) and len(x)>=6
POLICY = 2
MAX_GUESS_ALLOWED = int(1e1)
LIMIT = int(1e6)
# NUM_GUESSES = [1, 5, 10, 20, 30, 50, 100, 1000]
NUM_GUESSES = [1, 10, 100, 1000]
ALLOWED_EDITS = [
    'same',      # 1, 
    'swc-all',   # 2
    'rm-lasts',  # 3
    'swc-first', # 4
#    'cap2up',    # 5
    'upncap',  # 6
    'rm-lastd',  # 6
    'rm-lastl',  # 7
    'sws-last1', # 8
    'sws-lastn', # 9
    'rm-firstc', # 10
    'add1-last', # 11
#    'up2cap',    # 12
    'n2s-last',  # 13
]

TYPO_EDIT_DIST_LIMIT = 5
GENERAL = "GENERAL"
MOBILE = "MOBILE"
WHOLE=True

ONLY_DELTA = False
def get_policy_from_fname(fname):
    fname = os.path.basename(fname)
    m = re.search(r'Policy(?P<pnum>[0-3])', fname)
    if m:
        return int(m.group('pnum'))
    else:
        return self.POLICY

#!/usr/bin/env python3
"""Author: Rahul Chatterjee  

Description: The online attacker against typtop, can get all its guesses checked
against the stored typos and real password in the cache.  To estimate the
advantage of an onlnie attacker, we generate all cache instances via simulation,
and then run the greedy max cover algorithm.

"""
import build_typos as bt
from build_typos import (
    typtop_FUNCS, np,
    TYPTOP_CACHE_SIZE, CACHES,
    compute_cached_typo_dist_for_w, get_typof_name, readpw
)

from scipy import sparse
import marisa_trie
import os
from multiprocessing import Pool
import pandas as pd

pws, typos_trie = None, None
use_cached = True

def obtain_cache_distribution(typofname):
    """
    Creates a file containing all the caches, and their weights.
    :return:  Returns a array of caches.
    """
    bt.PREPROCESSED = True
    bt.RETURN_CACHE = True
    if bt.PW_TYPOS_DIST is None or len(bt.PW_TYPOS_DIST) <= 0:
        print("Loading typo array {}.npz".format(typofname))
        bt.PW_TYPOS_DIST = np.array([t for t in np.load(typofname + '.npz')['M'] if t is not None])
    print("Done loading the file. Total #typos: {}".format(bt.PW_TYPOS_DIST.shape))
    cache_dist_fname = typofname + '.cache.npz'
    if use_cached and os.path.exists(cache_dist_fname):
        return np.load(cache_dist_fname)['cachedist'].tolist()

    n_proc = 28
    p = Pool(28)
    nproc = p._processes
    if bt.N > 0:
        bt.N = min(len(bt.PW_TYPOS_DIST), bt.N)
    else:
        bt.N = len(bt.PW_TYPOS_DIST)
    epoch = bt.N//28
    print("bt.N = {}".format(bt.N))
    #  The RETURN_CACHE is set, so this will return n_sim x t x len(typo_FUNCS) array for each password
    cache_dist = np.array([
        w_cached_typo_dist for typo_ids, w_cached_typo_dist
        in p.map(compute_cached_typo_dist_for_w, range(bt.N), chunksize=epoch)
        if w_cached_typo_dist.shape[0] > 0
    ])
    # ret = [w_cached_typo_dist for typo_ids, w_cached_typo_dist
    #       in map(compute_cached_typo_dist_for_w, range(bt.N))]
    print("Waiting for the process to finish...")
    p.close()
    # cach_dist = np.array((bt.N, bt.NUM_SIMULATIONS, TYPTOP_CACHE_SIZE, len(typtop_FUNCS)))
    # cache_dist = np.array(ret)
    if cache_dist.shape != (bt.N, bt.NUM_SIMULATIONS, len(typtop_FUNCS), TYPTOP_CACHE_SIZE):
        print("Cache_dist shape did not match:\n\t{} != {}"\
              .format(cache_dist.shape, (bt.N, bt.NUM_SIMULATIONS, len(typtop_FUNCS), TYPTOP_CACHE_SIZE)))
    
    np.savez_compressed(cache_dist_fname, cachedist=cache_dist)


def read_typos_and_get_res(fname):
    """Reads the typo-files generated in the previous step, and run the greedy
    algorithm to find the maximum coverage.
    """
    global typos_trie, pws
    print("This tries to compute the edge weight (see CCS'17 paper by Chatterjee et al.)")
    print("Generating the guesses...")

    typofname = bt.get_typof_name(fname)
    # pws = readpw.Passwords(fname)  # trie of real passwords
    typos_trie = marisa_trie.Trie().load(typofname + '.trie')
    maxweight_res = pd.DataFrame(columns=typtop_FUNCS.keys())
    # Keeps track of the weight of the ball around a typo
    # (whose id according to typos_trie trie is i)
    cache_dist = np.load(typofname + '/')
    M = np.zeros((len(typos_trie), len(typtop_FUNCS)))
    epoch = 1000  # epoch for printing out the max ball weight
    for i, t in enumerate(obtain_cache_distribution(typofname)):
        typo_ids, cached_typo_dist = t
        if not np.any(typo_ids): continue
        M[typo_ids] += cached_typo_dist
        if i > 0 and i % epoch == 0:
            maxweight_res.loc[i] = [
                M[:, k].max() for k in range(len(typtop_FUNCS))
            ]
            # To not loose data in the middle.
            maxweight_res.to_csv("res_maxweight.csv")
    maxweight_res.to_csv("res_maxweight.csv")


def _guess_from_M(M_orig, p_orig, q, only_caches=CACHES):
    M = M_orig
    p = p_orig
    # blacklist = 100  # remove typos with prob more than p[blacklist, 1]
    assert np.all(p[:, 1]>=0)

    psorted = p[np.argsort(-p[:, 2])].copy()
    guesses = pd.DataFrame(columns=list(CACHES) + ['normal'])
    print("M.shape =", M)
    
    for ci, cache in enumerate(only_caches):
        print("Guessing....CAHCE ({}) = {}".format(ci, cache))
        cache_weights = np.repeat(p[:, 2], bt.NUM_SIMULATIONS)/bt.NUM_SIMULATIONS
        print(M[ci].shape, cache_weights.shape)
        for i in range(q):
            res = M[ci] * cache_weights
            # res[guesses[cache].astype(int)] = 0
            gi = np.argmax(res)
            guesses.loc[i, cache] = res[gi]
            print("Guess#{:3d}: {:8d} ({:18s}), prob={:.6f},  p({:18s})={:.6f}"
                  .format(
                      i, gi, typos_trie.restore_key(gi), res[gi],
                      typos_trie.restore_key(int(psorted[i, 1])), psorted[i, 2]
            ))
            cache_weights[M[ci][gi].nonzero()[1]] = 0
    guesses['normal'] = psorted[:q, 2]
    return guesses


def construct_M_matrix(typofname, only_caches=CACHES):
    """
    Tprob: probability of the typos according to pws
    cached_typo_dist:
    m_cache_dist = lil matrix n_pws x n_caches (x CACHES x t)
    ci: index in CACHES
    """
    fname = "/tmp/MTprobP_{}.npz".format('_'.join(only_caches))
    if use_cached and os.path.exists(fname):
        print("Reading from the cache file")
        t = np.load(fname)
        M, p = t["M"], t["p"]
        return M, p
    # else
    print("Could not find the cache file, so recreating")
    cache_dist = np.array([
        a
        for a in np.load(typofname + '.cache.npz')['cachedist']
        # if np.any(a)
    ])
    if bt.PW_TYPOS_DIST is None or len(bt.PW_TYPOS_DIST) <= 0:
        bt.PW_TYPOS_DIST = np.array([t for t in np.load(typofname + '.npz')['M'] if t is not None])
        
    pw2tpws = bt.PW_TYPOS_DIST
    print("Cache-dist (cache_dist) shape: {}".format(cache_dist.shape))
    print("typo-dist (T) shape: {}".format(pw2tpws.shape))

    # m_cache_dist is the same dimension as cache_dist, but contains real typo
    # id, as opposed to the index of the typo id in cache_dist. Note 'int16' ->
    # 'int32'.
    n_pws = pw2tpws.shape[0]
    n_cache = n_pws * bt.NUM_SIMULATIONS

    m_cache_dist = cache_dist
    for i in range(n_pws):
        m_cache_dist[i] = pw2tpws[i][m_cache_dist[i], 0].astype('int32')

    p = np.zeros((n_pws, 3))  # 0 -> pwid, 2 -> prob, 1 -> tpwid
    for i, a in enumerate(pw2tpws):
        pwid = a[0, 0]
        # print(pwid, pws.id2pw(pwid))
        tpwid = typos_trie.get(pws.id2pw(pwid))
        f = pws.id2freq(int(pwid))
        p[i, :] = [pwid, tpwid, f]

    p[:, 2] /= p[:, 2].sum()

    M = np.array([
        _get_M_matrix_for_ci(m_cache_dist, p, ci).tocsr()
        for ci, cache in enumerate(CACHES)
        if cache in only_caches
    ])
    print("Saving into the cache file.")
    np.savez_compressed(fname, M=M, p=p)
    return M, p

def _get_M_matrix_for_ci(m_cache_dist, p, ci):
    n_pws = m_cache_dist.shape[0]
    n_cache = n_pws * bt.NUM_SIMULATIONS
    # M contains, typo -> list of caches for each cache
    M = sparse.lil_matrix((len(typos_trie), n_cache), dtype=bool)
    _t = m_cache_dist[:,:,ci,:].reshape((n_cache, TYPTOP_CACHE_SIZE))
    M[_t.flatten(), np.repeat(np.arange(_t.shape[0]), _t.shape[1])] = True

    # The real password is always in the cache.
    for i, tpwid in enumerate(p[:, 1]):
        tpwid = int(tpwid)
        M[tpwid, i*bt.NUM_SIMULATIONS:(i+1)*bt.NUM_SIMULATIONS] = True
    return M


def make_online_guess(fname, cp, q):
    """
    fname(str) : name of the file to process, e.g.,
                 ~/passwords/rockyou-withcout.txt.gz, or something
    cp (str) : cache_policy, // LFU, PLFU, LRU, MFU
    q (int) :  number of guesses to output
    T (np.array(object)):  Array of typos for each password
    Tprob (np.array(, 2)): Array of pwindex and their probabilities.
    """
    global typos_trie, pws
    typofname = get_typof_name(fname)
    pws = readpw.Passwords(fname)  # trie of real passwords
    typos_trie = marisa_trie.Trie().load(typofname + '.trie')
    assert cp in CACHES
    ci = CACHES.index(cp) + 1
    M, p = construct_M_matrix(typofname, [cp])
    guesses = _guess_from_M(M, p, q, [cp])
    guesses.to_csv('guesses_online.csv', index_label='q')


if __name__ == "__main__":
    myparser = bt.argument_parser()
    args = myparser.parse_args()
    print(args)
    if args.fresh:
        for f in ["/tmp/MTprobP.npz"]:
            if os.path.exists(f): os.remove(f)
        bt.create_typos(args.pwfile, num_pws=args.numpws,
                        random_sample=(args.sample == 'random'))

        use_cached = False

    if args.attack not in ('online', 'on'):
        myparser.print_help()
        exit(-1)
    bt.NUM_SIMULATIONS = 500
    typofname = bt.get_typof_name(args.pwfile)
    obtain_cache_distribution(typofname)
    make_online_guess(args.pwfile, 'PLFU', 100)

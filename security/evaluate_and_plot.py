from pwmodel import PWModel
import config
import json, os, sys
import csv
from collections import OrderedDict
from pprint import pprint
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from itertools import cycle
# plt.rc('text', usetex=True)
# plt.rc('font', family='serif', size=12)
# plt.ioff()
import re, math, helper, itertools

def is_strict_increasing(A, name):
    """assert every row every column is stricly increasing"""
    for i, ai in enumerate(A):
        for j, aij in enumerate(ai[:-1]):
            if i<=0 or j<=0: continue
            if i>1: 
                assert A[i][j] >= A[i-1][j], "({}) Failed at i={}, j={}, Aij={}, A[i-1][j]={}"\
                    .format(name, i, j, A[i][j], A[i-1][j])
            if j>1:
                assert A[i][j] >= A[i][j-1], "({}) Failed at i={}, j={}, Aij={}, A[i][j-1]={}"\
                    .format(name, i, j, A[i][j], A[i][j-1])
    print "All rows and columns are non-decreasing!!"

def evaluate_guesses(aserver, guess_list):
    """
    auth_server is a pw model, policy is a Edit class or subclass of that
    guess_list is a list of guesses
    returns the additional success freq for each of the guesses in order
    """
    print "****** Evaluating using: ", A ,"for",name, "*****"  
    done = []
    ret = []
    g10 = []
    for i,g in enumerate(guess_list):
        ball = [rpw for rpw in aserver.check(g) if rpw not in done]
        done.extend(set(ball))
        ret.append(sum(aserver.pwmodel.get(rpw) for rpw in ball))

    # if len(aserver.transform_list)<=2:
    #     print g10
    # print "For policy={}:\n\t\tcovered {}, mass: {} many passowrds. (num-guesses: {})"\
    #     .format(aserver, len(done), sum(ret[:10]), len(guess_list))

    return ret, done
    # done = set(d for d in done if aserver.pwmodel.get(d)>0)
    # print '--'*20
    # print str(aserver)
    # print "Total Success: {}".format(sum(aserver.pwmodel.get(c) for c in done))
    # print len(done), done
    # print '--'*20


def parse_guess_file(guesslist_file, pwmodel={}):
    guesses_dict = json.load(open(guesslist_file))
    leakname = guesses_dict.values()[0].get('leakname', 'rockyou')
    if leakname not in pwmodel:
        pwmodel[leakname] = PWModel(leakname=leakname, evaluation_model=config.WHOLE, 
                          limit=config.LIMIT, pw_filter=config.PW_FILTER)
    else:
        print "Found pwmode!! reusing"
    pwmodel = pwmodel[leakname]

    policy_num = config.POLICY
    m = re.match(r'.*Policy(?P<pn>[0-9]).*', guesslist_file)
    if m:
        policy_num = int(m.group('pn'))
    return guesses_dict, leakname, pwmodel, policy_num


def process_guess_file(guesslist_file, pwmodel={}):
    """
    Rows are the name of the new edit
    columns are the different beta values
    """
    num_guesses = config.NUM_GUESSES
    allowed_edits = config.ALLOWED_EDITS
    guesses_dict, leakname, pwmodel, policy_num = parse_guess_file(guesslist_file, pwmodel)

    A = [['' for j in range(len(num_guesses)+2)] for i in range(len(allowed_edits)+1)]
    names = []

    aserver = None
    A[0][0] = 'edits'
    for j,n in enumerate(num_guesses):
        A[0][j+1] = 'q{}'.format(n)
    A[0][-1] = 'typofix'

    for editname, v in guesses_dict.items():
        name = editname.split('_',1)
        name, i = name[1], int(name[0])
        edits = v['edits'][0]
        aserver = config.AuthServer(edits,
                                    policy_num, pwmodel)
        if "typo_fix_rate" in v:
            fixedtypo, ttypo, tcorrect = v['typo_fix_rate']
        else:
            fixedtypo, ttypo, tcorrect = aserver.typo_fix_rate()
        A[i][-1] = fixedtypo*100.0/ttypo  # typo_fix_rate
    
        guesses, _ = evaluate_guesses(aserver, v['Guesses'])
        # print i, ', '.join('%.2f' % f for f in guesses[:10])
        # print name
        total_freq = pwmodel._total_freq

        # A.append([math.log(1/sum(guesses[:n]), 2) for n in num_guesses])
        A[i][0] = name
        for j,n in enumerate(num_guesses):
            A[i][j+1] = sum(guesses[:n])*100 

    A = filter(lambda a: any(a), A)
    if config.ONLY_DELTA:
        for i, arow in enumerate(A[2:]):
            for j,a in enumerate(arow):
                if j==0: continue
                arow[j] -= A[1][j]
        for i, a in enumerate(A[1][1:]):
            A[1][i+1] = 0
    # csvwriter = csv.writer(sys.stdout)
    # csvwriter.writerow(first_row)
    # csvwriter.writerows(A)
    try:
        is_strict_increasing(A, leakname+str(policy_num))
    except:
        print "@!@!@!!!!@@@NOT ALL COLUMNS ARE non-decreasing@@@@!@!!!!!!!"
    return A, policy_num

        
def calculate_sec_vs_typo(gfname):
    from typoanalysis import typo_types as ttypes
    performance_budget = 5
    allowed_edits = config.ALLOWED_EDITS
    secmap = {}  # transforms to its security loss 
    guesses_dict, leakname, pwmodel, policy_num = parse_guess_file(gfname)
    typomap = ttypes.typo_fix_stat()
    for editname, v in guesses_dict.items():
        name = editname.split('_',1)[1]
        if name not in allowed_edits: 
            print "Skipping {} not in allowed_edits".format(name)
            continue
        edits = v.get('edits', '')
        if not edits:
            edits = [name]
        aserver = config.AuthServer(edits,
                                    policy_num, pwmodel)
        secmap[name] = sum(evaluate_guesses(aserver, v['Guesses'])[0])
    for k,v in secmap.items():
        if k=='same': continue
        secmap[k] = math.log(v/secmap['same'], 2)
    secmap['same'] = 0.0

    # print "Secmap: "
    # print json.dumps(secmap, indent=2)
    # print "\nTypomap: "
    # print json.dumps(typomap, indent=2)
    # print ""
    editgrp2sec_typo = {}
    for g in helper.getallgroups(allowed_edits[1:], performance_budget):
        editgrp2sec_typo['|'.join(g)] = [sum(secmap[e] for e in g),
                                         sum(typomap[e] for e in g)]
    # print "Edits2Sec_Typo: "
    # print json.dump(editgrp2sec_typo, open('a.json', 'w'), indent=2)
    
    A = []
    last_max = []

    print "typogroup,security,typofix"
    for n,a in sorted(editgrp2sec_typo.items(), key=lambda x: x[1][0]):
        if last_max:
            assert set(n.split('|')) != set(last_max[0].split('|')), "They should never be the same!!"\
                                        "n={}, last_max={}".format(n, last_max)
        if not last_max or a[1]>last_max[2]:
            last_max = [n, a[0], a[1]]
        else:
            last_max[1] = a[0]
        print '{},{},{}'.format(*last_max)


    

def process_guess_files(fnames):
    Aarr = []
    pwmodel = {}
    for fname in fnames:
        A, policy_num = process_guess_file(fname, pwmodel)
        for i,a in enumerate(A[0]):
            A[0][i] = 'p{}{}'.format(policy_num,a)
        for i,a in enumerate(A[1:]):
            A[i+1] = [a[0]] + ["%.4f" % x for x in a[1:]]
        if not Aarr: # first take the first one 
            Aarr = A
        else:   # and then extend
            for i, r in enumerate(A):
                Aarr[i].extend(r[1:]) 
    csvwriter = csv.writer(sys.stdout)
    csvwriter.writerows(Aarr)

    
def plot(guesslist_file):
    A = process_guess_file(guesslist_file)

    num_plots = len(config.NUM_GUESSES)
    colormap = plt.cm.gist_ncar
    # plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, num_plots)])
    markers = cycle([u'bo-', u'gv-', u'y^-', u'rs-', u'c>-', u'm8-', u's-', u'p-.', u'*-.', 
                     u'h-.', u'H-', u'D-', u'd-'])
    

    x_range = range(len(A)-1)   # np.arange(len(names)+1, 1.0)
    fig, ax1 = plt.subplots()
    ax1.set_xlabel(r'Allowed transforms (cumulative)', fontsize=14)
    ax1.set_ylabel(r"Change in $\lambda_{q}$ (%)", fontsize=14)

    for i, n in enumerate(config.NUM_GUESSES):
        yrange = [a[i+1] for a in A[1:]]
        # is_strict_increasing(a[::-1], "Num Guesses: %d" % n)
        # is_strict_increasing(yrange[::-1], "Num Guesses: %d" % n)
        ax1.plot(x_range, yrange, markers.next(), linewidth=2.0)
        # print names[i], ', ' .join(str(x) for x in yrange)
    ax1.legend([r"$q$=%d" % n for n in config.NUM_GUESSES], loc='best')
    plt.ylim(-0.05, 2.2)
    plt.xticks(x_range, [a[0] for a in A[1:]], rotation='30', ha='right', fontsize=11)

    ax2 = ax1.twinx()
    ax2.set_ylabel(r'Increase in number of logins (\%)')
    ax2.plot(x_range, [a[-1] for a in A[1:]], 'k--', linewidth=2.0)
    ax2.legend([r"% typo fixed"], loc='best')
    plt.ylim(-0.05, 2.2)
    # plt.ylim(-0.05, A[-1][-1]+0.5)
    plt.xlim(-0.5, len(A))

    plt.gcf().set_size_inches(13, 8);
    # plt.show()
    grph_fname_ = 'graph/beta_suc_%s%s%s.pdf' % (os.path.basename(guesslist_file).split('.')[0],
                                                 '_whole' if config.WHOLE else '_part',
                                                 '_delta' if config.ONLY_DELTA else '_cdf')

    print "writing to {}".format(grph_fname_)
    plt.savefig(grph_fname_)


def test_evaluate_guesses(gfname):
    allowed_edits = config.ALLOWED_EDITS
    policy_num = config.get_policy_from_fname(gfname)
    guesses_dict = dict(sorted(json.load(open(gfname)).items(),
                                      key=lambda x: int(x[0].split('_', 1)[0])))

    pwmodel = PWModel(leakname=guesses_dict.values()[0].get('leakname', 'rockyou'),
                      evaluation_model=False)
    keys = ['2_swc-all', '3_rm-lasts']
    inds = [int(x.split('_')[0]) for x in keys]
    aserver1 = config.AuthServer(allowed_edits[:inds[0]], policy_num, pwmodel)
    aserver2 = config.AuthServer(allowed_edits[:inds[1]], policy_num, pwmodel)
    print allowed_edits[inds[0]-1], allowed_edits[inds[1]-1]
    print keys

    l = 1000
    glist1 = guesses_dict[keys[0]]['Guesses'][:l]
    glist2 = guesses_dict[keys[1]]['Guesses'][:l]

    c1, c2 = 0, 0
    for i, t in enumerate(zip(evaluate_guesses(aserver1, glist1),
                              evaluate_guesses(aserver2, glist2))):

        c1 += t[0]
        c2 += t[1]
        diff = c1 - c2
        if diff>1e-9:
            print i, glist1[i], glist2[i], t, diff





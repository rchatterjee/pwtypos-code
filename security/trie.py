#!/usr/bin/env python3

import marisa_trie
import os, sys

T = marisa_trie.Trie(
    l[:-1]
    for l in sys.stdin
)
outfname = sys.argv[1]
if not outfname.endswith('.trie'):
    outfname += '.trie'
T.save(outfname)

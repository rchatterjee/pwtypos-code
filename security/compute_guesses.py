import json
from find_best_guesses import find_optimal_guesses, greedy_maximum_coverage_heap
from pwmodel import PWModel
import config
import multiprocessing, bisect, heapq
from edits.edits import Edits
from edits.authserver import AuthServer
import sys, os, math
import helper
import evaluate_and_plot
from collections import OrderedDict
import random

log2 = lambda x: math.log(x,2)

def call_parallel(args):
    if len(args)==2:
        (aserverparams, max_num_guesses) = args
        name = ''
    elif len(args)==3:
        (aserverparams, max_num_guesses, name) = args
    aserver = AuthServer(*aserverparams, pwmodel=EVAL_PWMODEL)
    allowed_edits = aserver.transform_list
    total_freq = PWMODEL.total_freq()
    if not name:
        name = '%d_%s' % (len(allowed_edits), allowed_edits[-1])
    howmuch = evaluate_and_plot.evaluate_guesses
    glist = zip(*find_optimal_guesses(name, max_num_guesses, aserver, PWMODEL))[0]
    glist = zip(glist, howmuch(aserver, PWMODEL, glist)[0])
    typo_fix_rate = aserver.typo_fix_rate()
    v  = { 
        "name": name,
        "guesses": glist,
        "total_freq": total_freq, 
        "unique_pws": len(PWMODEL),
        "typo_fix_rate": typo_fix_rate,
        "edits": aserverparams
    }
    # print '--'*20
    # print AUTH_SERVER
    # print "Total Success: {}".format(sum(PWMODEL.get(c) for c in killed_pw_set))
    # for pw in [u'BEAUTIFUL', u'CHERRY', u'CLAUDIA', u'KIMBERLY', u'KISSES', u'YELLOW']:
    #     assert not PWMODEL.get(pw)>0, "{} is in kill_pw_set, but has freq: {}"\
    #         .format(pw, PWMODEL.get(pw))
    # print len(killed_pw_set), killed_pw_set
    # print '--'*20
    return v


def secloss_with_q(fname):
    D = json.load(open(fname))
    n = len(D.values()[0]['guesses'])
    suc = {k: 0 for k in D.keys()}
    killed_sets = {k: set() for k in D.keys()}
    aservers = {}
    pwm = None
    CHECKER_PWMODEL = PWModel(leakname='rockyou', evaluation_model=True, pw_filter=config.PW_FILTER)
    for k,v in D.items():
        if not pwm:
            pwm = PWModel(leakname=v['leakname'], evaluation_model=True, pw_filter=config.PW_FILTER)
        aservers[k] = AuthServer(*v['aserver'], pwmodel=CHECKER_PWMODEL)
    
    res = ['q', 'chkexact']
    res.extend(suc.keys())
    print ','.join(res)
    exact_suc = 0.0
    pwm.sum_top_q(n+1)
    exact_arr = [x/pwm._total_freq for x in pwm.topq.values()]
    for i in xrange(1, n+1):
        exact_suc += exact_arr[i-1]
        for k,v in D.items():
            guesses = v['guesses']
            new_pws = aservers[k].get_ball(guesses[i-1])-killed_sets[k]
            weight = sum(pwm.get(pw) for pw in new_pws)
            suc[k] += weight
            killed_sets[k] |= new_pws
        
        res = [str(i), exact_suc]
        res.extend(suc.values())
        print ','.join(str(x) for x in res)


def dropbox_utility(): 
    expname, strategies = get_checkers_params()
    print  expname, strategies
    #ATTACKER_PWMODEL = PWModel(leakname=leakname, pw_filter=config.PW_FILTER)
    EVAL_PWMODEL = PWModel(
        leakname=leakname, evaluation_model=True, 
        pw_filter=config.PW_FILTER
    )  # eval model is always rockyou
    CHECKER_PWMODEL = PWModel(
        leakname='rockyou', evaluation_model=True, pw_filter=config.PW_FILTER
    )
    
    guesses_for_all_strategies = {}
    for name, st in strategies.items():
        aserver = AuthServer(st[0], st[1], CHECKER_PWMODEL)
        pass   ### TODO-coplete it
    
def get_checkers_params():
    strategies = {}# take top 5 or top 3
    top = {}
    top[5] = ['same', 'swc-all', 'swc-first','rm-lastc', 'rm-firstc', 'n2s-last']
    top[3] = ['same', 'swc-all', 'swc-first','rm-lastc']
    top[2] = ['same', 'swc-all', 'swc-first']

    # strategies['exact'] = (['same'], 1)
    policy_list = [1,2,5]
    corr_list = [2, 3, 5]
    for policy in policy_list:
        for c in corr_list:
            strategies["top%d-p%d"%(c, policy)] = (top[c], policy)

        # for p in top5:
        #     strategies[p] = ([p], 1) if p =='same' else (['same', p], 1)
        # strategies = {'top2p1': (top2, 1), 'top2p2': (top2, 2), 'top2p5': (top2, 5),
        #               'top3p1': (top3, 1), 'top3p2': (top3, 2), 'top3p5': (top3, 5),
        #               'top5p1': (top3, 1), 'top5p2': (top3, 2), 'top5p5': (top3, 5),
        #               }
    expname = 'top%sp%s'%(''.join(str(a) for a in corr_list),
                          ''.join(str(b) for b in policy_list))
    return expname, strategies


def measure_utility(leakname='rockyou'):
    test_pwmodel = PWModel(leakname=leakname, evaluation_model=True, 
                           pw_filter=config.PW_FILTER)
    N = 100000
    expname, strategies = get_checkers_params()
    policy = 'top3-p1'
    strategies = {policy: strategies[policy]}
    print  expname, strategies
    #### params #####
    failed_login = 0.33  # f = 1.0(3 + 8a)
    re_login_prob = 0.7  # 1-a
    failed_login = 1.0/(11 - 8*re_login_prob)
    success_prob = 1 - failed_login

    samples = helper.sample_following_dist(test_pwmodel, N, 1.0)

    typo_prob = OrderedDict([('swc-all', 0.0121 ), 
                             ('swc-first', 0.0570),
                             ('rm-lastc', 0.0211 ),
                             ('rm-firstc', 0.0036),
                             ('n2s-last', 0.0022)])

    typo_prob['failed_other'] = 1.0 - sum(typo_prob.values())

    sample_edits = list(helper.sample_following_dist(typo_prob.iteritems(), N*4, 1.0))
    random.shuffle(sample_edits)
    sample_edits = iter(sample_edits)
    s = 0
    E = Edits()
    aservers = {k: AuthServer(v[0], v[1], pwmodel=test_pwmodel) \
                for k,v in strategies.items()}
    aservers_cnts = {k: 0 for k in strategies}
    total_failed_login = 0
    total_successful_login = 0
    gaveup = 0
    corrected_users = set()
    for f, pw in samples:
        p = re_login_prob
        while True:
            if random.random()<success_prob:   # success full attempt -- kudos :)
                total_successful_login += 1
                break
            _, sample_edit = sample_edits.next()
            if sample_edit != 'failed_other':
                pwtypo = list(E.fast_modify(pw, [sample_edit], typo=True))
                if not pwtypo:
                    continue
                if len(pwtypo)<=1:
                    pwtypo = pwtypo[0]
                else:
                    pwtypo = random.choice(pwtypo)
                if pw == pwtypo:
                    print "It should not happen, but it did.", pw, pwtypo, sample_edit
                    continue
                if aservers[policy].check(pwtypo, pw):
                    aservers_cnts[policy] += 1
                    corrected_users.add(f)
                    total_successful_login += 1
                    
                # for k, aserver in aservers.items():
                #     if aserver.check(pwtypo, pw):
                #         aservers_cnts[k] += 1
            total_failed_login += 1
            if random.random()>p: 
                gaveup += 1
                break
            else:
                p/=1.0
    print json.dumps(aservers_cnts, indent=2)
    print "Total # failed Login:", total_failed_login
    print "Total # successful Login:", total_successful_login
    print "Total # gaveup:", gaveup
    nu = len(corrected_users)
    print "Percentage of typo corrected:", aservers_cnts[policy]*100.0/total_failed_login
    print "Percentage of login increased:", nu*100.0/N

def compute_guesses_for_all_strategies(leakname='rockyou'):
    global PWMODEL, MAX_GUESS_ALLOWED, EVAL_PWMODEL
    MAX_GUESS_ALLOWED = 1000
    print "Generating with respect to", leakname
    ATTACKER_PWMODEL = PWModel(leakname=leakname, pw_filter=config.PW_FILTER)
    EVAL_PWMODEL = PWModel(leakname=leakname, evaluation_model=True, 
                           pw_filter=config.PW_FILTER)  # eval model is always rockyou
    CHECKER_PWMODEL = PWModel(leakname='rockyou', evaluation_model=True, pw_filter=config.PW_FILTER)

    expname, strategies = get_checkers_params()
    print  expname, strategies
    
    guesses_for_all_strategies = {}
    for name, st in strategies.items():
        aserver = AuthServer(st[0], st[1], CHECKER_PWMODEL)
        guesses_for_all_strategies[name] = {
            #'guesses': find_optimal_guesses(name, MAX_GUESS_ALLOWED, aserver, ATTACKER_PWMODEL),
            'guesses': [g for g,w in greedy_maximum_coverage_heap(name, MAX_GUESS_ALLOWED, aserver, ATTACKER_PWMODEL)],
            'fixrate': [0, 1, 2], #aserver.typo_fix_rate(),
            'aserver': st,
            'leakname': leakname
        }
        assert len(guesses_for_all_strategies[name]['guesses'])==MAX_GUESS_ALLOWED,\
            "Length of the guesses ({}) is not equalto expected = {}" \
            .format(len(guesses_for_all_strategies[name]['guesses']), MAX_GUESS_ALLOWED)

    outfname = "data/all_%s_%s.json" % (expname, leakname)
    json.dump(guesses_for_all_strategies, open(outfname,'w'), indent=2, sort_keys=True)

    res = {}
    exact_successrate = EVAL_PWMODEL.sum_top_q(MAX_GUESS_ALLOWED)
    print "\n********************************************\n"
    print ">> RESULTS:", leakname
    print ','.join(['name', 'typo_fix_rate', 'ExactSuccess', '\\fuzzlambda_beta'])
    for name, v in guesses_for_all_strategies.items():
        aserver = AuthServer(*v['aserver'], pwmodel=CHECKER_PWMODEL)
        tfrate = v['fixrate']  # fixed, total_typo, total_typed
        tfrate = tfrate[0]*100.0/(tfrate[-1]-tfrate[1])
        success = sum(EVAL_PWMODEL.get(pw) for pw in aserver.get_ball_union(v['guesses']))
        delta_q = success - exact_successrate
        print ','.join([name, str(tfrate), str(exact_successrate), str(delta_q)])


def subopt_attacker_success(outfname):
    # outfname = "data/all_strategy_result_%s.json" % leakname
    MAX_GUESS_ALLOWED = 1000
    guesses_for_all_strategies = json.load(open(outfname))
    leakname = guesses_for_all_strategies.values()[0]['leakname']
    CHECKER_PWMODEL = PWModel(leakname='rockyou', evaluation_model=True, pw_filter=config.PW_FILTER)
    attackerpwm = PWModel(leakname=leakname, evaluation_model=True, 
                                       pw_filter=config.PW_FILTER)
    n = len(guesses_for_all_strategies.values()[0]['guesses'])
    attackerpwm.sum_top_q(MAX_GUESS_ALLOWED)
    topq = attackerpwm.topq.items()
    # print topq
    print "\n********************************************\n"
    print ">> RESULTS:", leakname
    # ATTACKER_PWMODEL = PWModel(leakname=leakname, pw_filter=config.PW_FILTER)
    print ','.join(['name', 'typo_fix_rate', '\\fuzzlambda_beta'])
    for leakname in ['rockyou', 'myspace', 'phpbb']:
        EVAL_PWMODEL = PWModel(leakname=leakname, evaluation_model=True, pw_filter=config.PW_FILTER) 
        print "This time pwm=",leakname
        exact_successrate = sum(EVAL_PWMODEL.get(pw) for pw,f in topq)
        print "Exact: ", len([g for g,f in topq if EVAL_PWMODEL.get(g)>0])
        for name, v in guesses_for_all_strategies.items():
            aserver = AuthServer(*v['aserver'], pwmodel=CHECKER_PWMODEL)
            tfrate = v['fixrate']  # fixed, total_typo, total_typed
            tfrate = tfrate[0]*100.0/(tfrate[-1]-tfrate[1])
            guesses = aserver.get_ball_union(v['guesses'][:MAX_GUESS_ALLOWED])
            # print guesses
            # print v['guesses'][:MAX_GUESS_ALLOWED]
            # killed_pw_set = aserver.get_ball_union(v['guesses'])
            print len([g for g in guesses if EVAL_PWMODEL.get(g)>0])
            #print exact_successrate
            success = sum(EVAL_PWMODEL.get(pw) for pw in guesses)
            #print success, exact_successrate
            delta_q = success-exact_successrate
            print ','.join([name, str(exact_successrate), str(success), str(delta_q)]) 
        print "\n"
    
def glist2deltaq(outfname):
    # outfname = "data/all_strategy_result_%s.json" % leakname
    ComputeTFrate=False
    MAX_GUESS_ALLOWED = 100
    guesses_for_all_strategies = json.load(open(outfname))
    leakname = guesses_for_all_strategies.values()[0]['leakname']

    # ATTACKER_PWMODEL = PWModel(leakname=leakname, pw_filter=config.PW_FILTER)
    EVAL_PWMODEL = PWModel(leakname=leakname, evaluation_model=True,
                           pw_filter=config.PW_FILTER) 
    CHECKER_PWMODEL = PWModel(leakname=leakname,
                              evaluation_model=True,
                              pw_filter=config.PW_FILTER)


    exact_successrate = EVAL_PWMODEL.sum_top_q(MAX_GUESS_ALLOWED) 
    print EVAL_PWMODEL
    print "\n********************************************\n"
    print ">> RESULTS:", leakname
    print ','.join(['name', 'typo_fix_rate', '\\fuzzlambda_beta'])
    print "Exact Success:",  exact_successrate*100.0
    res = {}
    for name, v in guesses_for_all_strategies.items():
        aserver = AuthServer(*v['aserver'], pwmodel=CHECKER_PWMODEL)
        # tfrate = v['fixrate']  # fixed, total_typo, total_typed
        tfrate = -1.0
        if ComputeTFrate:
            tfrate = aserver.typo_fix_rate()
            tfrate = tfrate[0]*100.0/(tfrate[-1]-tfrate[1])
        guesses = aserver.get_ball_union(v['guesses'][:MAX_GUESS_ALLOWED])
        assert len(guesses)>=MAX_GUESS_ALLOWED, "The number of guesses ({}) is less than expected=({})"\
            .format(len(guesses), MAX_GUESS_ALLOWED)
        killed_pw_set = aserver.get_ball_union(v['guesses'])
        # print len(killed_pw_set)
        success = sum(EVAL_PWMODEL.get(pw) for pw in guesses)
        # print success, exact_successrate
        delta_q = success-exact_successrate
        # print ">>>>>", ','.join(str(s) for s in [name, tfrate, success, exact_successrate, delta_q]) 
        res[name] = delta_q*100.0
    for top in ['top2', 'top3', 'top5']:
        print ' & '.join("{0:.2f}".format(res[top+'-p{}'.format(p)]) for p in [1,2,5]), '\\\\'
    

def find_all_guesses(real_model_leak, attacker_model_leak=None, policy=3):
    global MAX_GUESS_ALLOWED, PWMODEL
    MAX_GUESS_ALLOWED = 10000
    if not attacker_model_leak:
        attacker_model_leak = real_model_leak

    ATTACKER_PWMODEL = PWModel(leakname=attacker_model_leak, pw_filter=config.PW_FILTER)
    EVAL_PWMODEL = PWModel(leakname=real_model_leak, evaluation_model=True, 
                           pw_filter=config.PW_FILTER)  # eval model is always rockyou
    CHECKER_PWMODEL = PWModel(leakname='rockyou', evaluation_model=True, pw_filter=config.PW_FILTER)
    num_guesses = config.NUM_GUESSES
    MAX_GUESS_ALLOWED = max(num_guesses)

    allowed_edits = ['same', 'swc-all', 'swc-first', 'rm-lastc', 
             'rm-firstc', 'sws-last1', 'upncap',
             'add1-last']
    top2 = allowed_edits[:3]

    num_edits = len(allowed_edits)
    # pool = multiprocessing.Pool(4)
    # guesses_for_all_edits = pool.map(call_parallel,
    #                                  [((config.ALLOWED_EDITS[:i],
    #                                     policy), max(num_guesses)) for i
    #                                   in xrange(1, num_edits+1, 1)])
    guesses_for_all_edits = {}
    exact_successrate = EVAL_PWMODEL.sum_top_q(MAX_GUESS_ALLOWED)
    outstr = []
    for i in xrange(7, num_edits,1):
#    for i in [4]: # xrange(1, num_edits+1,1):  # --change--
        name = allowed_edits[i-1]
        aserver = AuthServer(allowed_edits[:i], policy_num=5, pwmodel=CHECKER_PWMODEL)
        guesses, = find_optimal_guesses(name, MAX_GUESS_ALLOWED, aserver, ATTACKER_PWMODEL),
        fixrate = aserver.typo_fix_rate()
        guesses_for_all_edits[name] = {
            'guesses': guesses,
            'fixrate': fixrate,
            'aserver': [allowed_edits[:i], 3],
            'real_leak': real_model_leak,
            'attack_leak': attacker_model_leak
        }
        success = sum(EVAL_PWMODEL.get(pw) for pw in aserver.get_ball_union(guesses))
        delta_q = log2(success/exact_successrate)
        outstr.append("{},{},{}".format(allowed_edits[i-1], delta_q, fixrate))
        print "------------>",  outstr[-1]
    print '\n'.join(outstr)
    
    guess_fl = 'data/Guesses_%s_%d_PW%g_Policy%d.txt' % \
               (PWMODEL.leakname, MAX_GUESS_ALLOWED, LIMIT, config.POLICY)
    print "Wrting to file: {}".format(guess_fl)
    with open(guess_fl, 'wb') as f:
        json.dump(G, f, indent=4, sort_keys=True)
    # evaluate_and_plot.plot(guess_fl)

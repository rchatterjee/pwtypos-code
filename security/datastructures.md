# Details of all the datastructures:
The file uses several custom data structures for performance.
The detailed list is here. 


* `pws`: a database of passwords build using `Passwords` class in `readpw.py`.
* `typos_trie`, a trie containing all the typos. 
* `typodist`
* `cachedtypodist`
* `GZIP_LINES` : Data in `<typofname>.npz`, which is just neighborhood of each password. 

## Files stores

* `<typofname>.trie` : A simple trie fo typos, this file is read into `typos_trie`.  

* `<typofname>.npz`: For each passwored there is a row containing `typo_id` and
  their probability. The id is obtained from the `typos_trie`. The number of
  rows is equal to the number of password considered. 
  ```python
   typo_f = np.load('<typofname>.npz`)['M']
  ```

* `<typofname>.both.npz`: 




## Results

`NUM_SIMULATIONS = 500`, `TRANSCRIPT_SIZE = 200`.  
       q       LFU      PLFU       LRU       MFU    normal
10    55  0.094897  0.094802  0.094339  0.094990  0.094339
99  4950  0.209681  0.209237  0.204228  0.211299  0.204228
=delta=     0.0054    0.0050   0.00000    0.0707


`NUM_SIMULATIONS = 1000`, `TRANSCRIPT_SIZE = 200`.  
       q       LFU      PLFU       LRU       MFU    normal
10    55  0.094885  0.094763  0.094339  0.094986  0.094339
99  4950  0.209750  0.209135  0.204228  0.211263  0.204228
=delta=    0.00552   0.00492   0.00000   0.00700

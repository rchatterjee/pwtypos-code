#!/bin/bash

for leakfile in ~/passwords/phpbb-withcount.txt.bz2 ~/passwords/myspace-withcount.txt.bz2 ~/passwords/weirpcfg-withcount.txt.bz2 ~/passwords/rockyou-withcount.txt.bz2
do 
    leakname=`basename $leakfile | cut -d'-' -f1`
    echo $leakfile, $leakname
    for policy in 4; 
    do 
	echo "POLICY: $policy"; 
	python main.py -guess $policy $leakfile; 
    done > output_${leakname} 2>&1
done

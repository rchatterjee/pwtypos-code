
# list of correctors, name -> function, 
# function: tpw -> {rpw_1, rpw_2,...} 
CORRECTORS = {
    
}

def get_typos(w):
    typos = {
        'capslock': w.swapcase(),
        'shift': w[1].swapcase() + w[1:],
    }
    # remove which are the same as w
    for k in typos:
        if typos[k] == w:
            typos[k] = ''
    return typos

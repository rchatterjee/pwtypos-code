#!/usr/bin/python
import dataset
import json
import csv
import Levenshtein as lv
import os
from numpy.random import randint, choice, shuffle
import logging
logging.basicConfig()
import dbaccess
from mturk import mturk_utils
from collections import defaultdict
from Levenshtein import distance as edist
import re 


db = dataset.connect('mysql://localhost/pwtypos')
import datetime 
def now():
    datetime.datetime.now().isoformat()


def check_capslock_typo():
    T = db.query("SELECT T.assignmentId, count(*) as c, capslock from "
                      "(SELECT assignmentId, capslock from pwresults where tpw!='' and "
                      "pwid<='pwid000100000' and rpw collate latin1_general_cs != tpw and "
                      "rpw=tpw) as T group by assignmentId")
    A = defaultdict(int)
    invalid_pwids = []
    for i, t in enumerate(T):
        aid, c = t['assignmentId'], t['c']
        rpwtpw = list(db.query("select pwid, rpw, tpw from pwresults where "
                          "assignmentId = '{}';".format(aid)))
        # find the actual number of caps lock typo
        capslock = 0
        isfirst = True
        for d in rpwtpw:
            rpw, tpw = d['rpw'], d['tpw']
            if rpw==tpw:
                if re.findall(r'[a-zA-Z]', tpw):
                    isfirst = True
                    if tpw.isupper():
                        capslock = 1
                    elif tpw.islower():
                        capslock = 0
            else:
                if capslock==1:
                    if isfirst:
                        isfirst = False
                    else:
                        tpw = tpw.swapcase()
                elif rpw.swapcase() == tpw:
                    isfirst = False
                    capslock = 1-capslock

            print ','.join([d['pwid'], rpw, tpw])
        if (i%100)==0:
            print "Done:", i
        A[aid] = rpwtpw
    open('invalidpwids.txt', 'w').write('\n'.join(invalid_pwids))

    json.dump(A, open('capslocissue.json', 'w'), indent=2)
    return A

def toobad_pw():
    f = open('invalidpwids1.txt', 'a')
    for row in db.query('select pwid, rpw, tpw from pwresults where tpw!=rpw'):
        if edist(row['tpw'].lower().strip(), row['rpw'].lower().strip())>5:
            f.write(row['pwid'] + '\n')

def capslock_error_groups():
    T = json.load(open('capslocissue.json'))
    err_d = {'start_wrong': 0, 'uppercase_induced': 0, 'fixed_in_midle': 0, 'weird': 0, 'all_wrong': 0}
    print "Total aids:", len(err_d)
    capserr_cnt = set()
    
    for k, v in T.items():
        upper_case = 0
        pos = 0
        done = 0
        if all(map(lambda x: x['rpw'].swapcase() == x['tpw'], v)):
            err_d ['all_wrong'] += 1
            continue
        for d in v:
            pwid, tpw, rpw = d['pwid'], d['tpw'], d['rpw']
            assert pwid<="pwid000100000", "Pwid={} is too big".format(pwid)
            if not re.findall(r'[a-zA-Z]', rpw):
                print pwid, rpw, tpw
                continue
            if rpw.isupper():
                upper_case = 1
            else:
                upper_case = 0
            if done==0 and rpw.swapcase() == tpw:  # caps lock error
                capserr_cnt.add(pwid)
                if pos==0:
                    err_d['start_wrong'] += 1
                    print k
                elif upper_case==1:
                    err_d['uppercase_induced'] += 1
                else:
                    err_d['weird'] += 1
                done = 1
            elif rpw==tpw:
                if done==1:
                    err_d['fixed_in_midle'] += 1
                    done=2
            pos += 1
    print json.dumps(err_d)
    print "Total CapsErrors:", len(capserr_cnt)
def fix_capslock_shit():
    for pwid in open(sys.argv[1]).read():
        pwid = pwid.strip()
        db.query("update pwresults set isvalid=0 where pwid='{}';".format(pwid))

def insert_complex_pw():
    fname = 'data/complex_pwset_5K_each.json'
    D = json.load(open(fname))
    pwtable = db["pwresults"]
    def create_pw_entry():
        cnt = 0
        for k,v in D.items():
            pwid = k.replace('pwid000', 'pwid002')   # pwid002 to signify that it is a complex pw set
            cnt += 1
            if cnt%1000==0:
                print "Done", cnt
            yield ({"pwid": pwid,
                    "rpw": str(v),
                    "tpw": '',
                    "ttpw": '',
                    "assignmentId": '',
                    "capslock": '',
                    "date": now(),
                    "exptype": 'GENERAL',
                    "workerId": '',
                    "status": "Available"  # Available, Submitted, Posted
                })
    pwtable.insert_many(create_pw_entry())

def setup_pwresults():
    pwtable = db.get_table("pwresults", primary_id="pwid", primary_type="String(30)")
    oldpwtable = db['typedresultsgeneral']

    import itertools
    base_dir = "/home/rahul/dsss/code_old/"     # the worst way to code, but I dont have time
    pwdataset = json.load(open('{base_dir}/mturk/pwset/S1_b4b37058387df'.format(base_dir=base_dir)))
    num_rows_done = 0
    for pwid, rpw in pwdataset.items():
        if num_rows_done %1000==0:
            #db.begin()
            pass
        rows = list(oldpwtable.find(pwid=pwid))
        assert len(rows)<=1, "There are multiple rows with same pwid={}".format(pwid)
        present = len(rows)
        if len(rows)==1:
            rows = rows[0]
        else:
            rows = {}
        pwtable.insert({"pwid": pwid,
                        "rpw": rpw,
                        "tpw": rows.get("tpw", ''),
                        "ttpw": rows.get('ttpw', ''),
                        "assignmentId": rows.get('assignmentId', ""),
                        "capslock": rows.get('capslock', ""),
                        "date": rows.get('data', now()),
                        "exptype": rows.get('exptype', ''),
                        "status": ["Available", "Submitted"][present]  # Available, Submitted, Posted
                    }, ['pwid'])
        print num_rows_done
        num_rows_done += 1
        if (num_rows_done %1000 == 0 ):
            pass
            # db.commit()
            print "Done: {} rows".format(num_rows_done)
    pwtable.create_index(['pwid', 'assignmentId'])
    


def setup_hitresults():
    hittable = db.get_table("hittable", primary_id="assignmentId", primary_type="String(50)")
    oldhitresults = db['hitresults']
    import itertools
    for row in oldhitresults:
        keys = [("assignmentId", "AssignmentId"),
                ("submitTime",       "SubmitTime"),
                ("workerId",         "WorkerId"),
                ("answers",          "answers"),
                ("acceptTime",       "AcceptTime"),
                ("hitId",            "HITId"),
                ("assignmentStatus","AssignmentStatus"),
                ("title",            "Title"),
                ("creationTime",     "CreationTime"),
                ("reward",           "Reward"),
                ("hitGroupId","HITGroupId"),
                ("pwids", "pwids"),
                ("userAgent", "userAgent"),
                ("exptype", "exptype"),
                ("capslock", "capslock")
            ]
        del row['id']
        new_row = {k: row.get(oldk, '') for k, oldk in keys}
        hittable.insert(new_row, ['assignmentId'])
    hittable.create_index(['assignmentId'])


def fix_pwids_in_hittable():
    hittable = db.get_table("hittable", primary_id="assignmentId", primary_type="String(50)")
    num_done = 0
    for row in hittable:
        ans = row['answers'].strip()
        if ans and not row['pwids']:
            ansres = list(dbaccess.parse_answer_field(row))
            if not ansres:
                print "Deleting: {}. No pw could be recovered!".format(row['assignmentId'])
                hittable.delete(assignmentId=row['assignmentId'])
            else:
                dbaccess.update_pwresults(row, ansres)
                pwids = [v['pwid'] for v in ansres]
                row['userAgent'] = dbaccess.get_user_agent(row)
                row['pwids'] = '|'.join(pwids)
                hittable.upsert(row, ['assignmentId'])
        num_done += 1
        if num_done %100 == 0:
            print num_done

def fix_user_agent():
    hittable = db.get_table(
        "hittable", primary_id="assignmentId", primary_type="String(50)"
    )
    num_done = 0
    for row in hittable:
        ans = row['answers'].strip()
        changed = False
        if row['assignmentStatus'] == 'Submitted':
            print "fixing {assignmentStatus}".format(**row)
            # fix the assignment Status
            try:
                row['assignmentStatus'] = mturk_utils.get_assignment_status(row['assignmentStatus'])
            except:
                row['assignmentStatus'] = "Approved"
            changed = True
        if ans and not row['userAgent']:
            ansres = list(dbaccess.parse_answer_field(row))
            if not ansres:
                print "Deleting: {}. No pw could be recovered!".format(row['assignmentId'])
                hittable.delete(assignmentId=row['assignmentId'])
            else:
                row['userAgent'] = dbaccess.get_user_agent(row)
                changed = True
        if changed:
            hittable.upsert(row, ['assignmentId'])
        num_done += 1
        if num_done %100 == 0:
            print num_done

def fix_pwstatus():
    pwresultstable = db["pwresults"]
    hittable = db.get_table(
        "hittable", primary_id="assignmentId", primary_type="String(50)"
    )
    for row in hittable['pwids', 'workerId']:
        pwids, workerId = row['pwids'], row['workerId']
        if workerId and pwids and len(workerId)>5 and len(pwids)>4:
            for pwid in pwids.split('|'):
                db.query("update pwresults set workerId='{}' where pwid='{}'"\
                         .format(workerId, pwid))
                 
if __name__ == "__main__":
    # setup_pwresults()
    # setup_hitresults()
    # fix_pwids_in_hits()
    insert_complex_pw()
    pass

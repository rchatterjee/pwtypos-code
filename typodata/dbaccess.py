#!/usr/bin/python
import dataset
import json
import csv
import Levenshtein as lv
import os
from numpy.random import randint, choice, shuffle
import logging
logging.basicConfig()
from security.config import *
from hashlib import sha256 as sha2

db = dataset.connect('mysql://localhost/pwtypos')
pwresultstable = db['pwresults']
hittable = db['hittable']
workertable = db['workers']

def exptype(pwid, A):
    for k in A:
        if pwid in A[k]:
            return k
    return "NOTKNOWN"

def get_worker_assignment_count(workerId):
    return hittable.count(workerId=workerId)

def get_user_agent(row):
    for ans in json.loads(row['answers']):
        u = ans.get("userAgent", [''])[0]
        return ans.get("userAgent", [''])[0]

    
def parse_answer_field(row):
    for ans in json.loads(row['answers']):  # there could be multiple forms
        try:
            s = json.loads(ans['keypressData'][0])
        except ValueError:
            s = {}
            continue
        if "RPWS" in s:
            for pwid_rpw, tpw in zip(s['RPWS'], ans['pass'][0].split('|')):
                pwid, rpw = pwid_rpw
                ttpw = json.dumps(s.get(pwid, []))
                tpw = tpw.encode('ascii', 'ignore')
                ttpw = ttpw.encode('ascii', 'ignore')
                yield {
                    "pwid": pwid,
                    "exptype": 'NOTKNOWN',
                    "rpw": rpw,
                    "tpw": tpw,
                    "ttpw": ttpw,
                    "workerId": row['workerId'],
                    "assignmentId": row['assignmentId'],
                    "capslock": int(ans.get('capsLockVar', ['-1'])[0])
                }
        else:
            for pwid, d in json.loads(ans['keypressData'][0]).items():  # only care about the keypressData
                try:
                    yield {
                        "pwid": pwid,
                        "exptype": 'NOTKNOWN',
                        "rpw": d['rpw'].encode('ascii', 'ignore'),
                        "tpw": d['tpw'].encode('ascii', 'ignore'),
                        "ttpw": json.dumps(d['ttpw']),
                        "workerId": row['workerId'],
                        "assignmentId": row['assignmentId'],
                        "capslock": int(ans.get('capsLockVar', ['-1'])[0])
                }
                except:
                    continue

def pwresults_data(type_ = 'popular'):
    pwid_limit = {
        'popular': "pwid <= 'pwid000100000'",
        'complex': "pwid >= 'pwid000200000' and pwid <= 'pwid002020000'"}\
        .get(type_, '')
    all_valid_entries = db.query("SELECT * from pwresults where tpw!='' and "\
                                     "{} order by pwid"\
                                     .format(pwid_limit))
    override_pws = {}
    for line in open('err_pws'):
            try:
                pwid, rpw, tpw = line.strip().split(',',2)
            except:
                print "ERRRRRRRRRR", line
                exit(0)
            override_pws[pwid] = (rpw, tpw)
    for row in all_valid_entries:
        if row['pwid'] in override_pws:
            row['rpw'], row['tpw'] = override_pws[row['pwid']]
        yield row
                    
def acceptable(row):
    """                                                                                             
    :param row: a data base row having 'rpw', 'tpw' and 'ttpw'                                      
    :return: if the row is an acceptable submission, i.e., tpw and ttpw are not null, and the       
    edit distance is within 30% of the length                                                       
    """                                                                                             
    if not row:                                                                                     
        return False                                                                                
    rpw, tpw, ttpw = row.get('rpw', '').strip(), row.get('tpw', '').strip(), row.get('ttpw', '{}')  
    capslock = row['capslock']                                                                      
    if capslock < 0:                                                                                
        return False                                                                                
    if not ttpw:                                                                                    
        return False                                                                                
    ttpw = json.loads(ttpw)                                                                         
    if rpw and tpw and ttpw:                                                                        
        return True                                                                                 
    return False                                                                                    

def update_pwresults(hitrow, ansres=None):
    if not ansres and hitrow['answers'].strip():
        ansres = parse_answer_field(hitrow)
    if not ansres:
        print "No answer found in {}.\n Ignoring".format(hitrow['answers'])
    return_pwids, useragent = [], ''
    for pwset in ansres:
        pwrow = pwresultstable.find_one(pwid=pwset['pwid'])
        if not pwrow:
            continue
        return_pwids.append(pwset['pwid'])
        if not useragent:
            useragent = ''
        pwset['exptype'] = pwrow.get('exptype', 'NOTKNOWN')
        if pwrow['status'] == "Approved" and acceptable(pwrow) and pwrow['exptype'] == 'GENERAL':
            print "Already exist so ignoring! {}".format(pwrow['pwid'])
        else:
            pwrow['status'] = "Approved"
            pwrow.update(pwset)
        pwresultstable.upsert(pwrow, ['pwid'])
    return {"pwids": return_pwids, 
            "capslock": pwset['capslock']}


def get_posted_hits():
    for hit in db.query("select hitId from hittable where assignmentStatus collate latin1_general_cs = 'Posted'"):
        yield hit['hitId']



def update_hit(result):
    hitrow = hittable.find_one(hitId=result['hitId'])
    if not hitrow:
        "Could not find entry for this hit, creating one"
        hitrow = {}
    hitrow.update(result)
    d = update_pwresults(hitrow)
    pwids, capslock = d['pwids'], d['capslock']
    if hitrow['assignmentStatus'] == 'Submitted':
        hitrow['assignmentStatus'] = 'Approved'
    if not hitrow['pwids']:
        hitrow['pwids'] = pwids
    hittable.upsert(hitrow, ['hitId'])



def create_hit(hitid, pwids, exptype):
    """
    ASSUMES NEW, WILL not check for existing hitid
    """
    random_aid = '--{}{}'.format(hitid[:5], sha2(hitid+str(pwids)).hexdigest()[:5]).upper()
    hitrow = {'hitId': hitid, 'assignmentId': random_aid, 'expttype': exptype, 
              'assignmentStatus': 'Posted', 'pwids': pwids}
    hittable.insert(hitrow, ['hitId'])


#### Not checked ##########
def get_data_for_pwid(pwid):
    table = open_db_table()
    for row in table:
        if pwid in row['answers']:
            return json.dumps(row, indent=2)


def get_rpw_tpw_pairs(exptype=GENERAL):
    table = open_db_table()
    if not os.path.exists(PWID_CACHE_FILE):
        mobile_general_input_cache()

    A = json.load(open(PWID_CACHE_FILE))
    for k, v in A.items():
        A[k] = set(v)
    for row in table:
        if 'answers' not in row:
            print row
            exit(0)
        for ans in json.loads(row['answers']):  # there could be multiple forms
            s = json.loads(ans['keypressData'][0])
            if "RPWS" in s:
                for pwid_rpw, tpw in zip(s['RPWS'], ans['pass'][0].split('|')):
                    pwid, rpw = pwid_rpw
                    ttpw = ''.join(x[0] for x in s.get(pwid, []))
                    if pwid in A[exptype]:
                        yield pwid, rpw, tpw, ttpw
            else:
                for pwid, d in json.loads(ans['keypressData'][0]).items():  # only care about the keypressData
                    if pwid in A[exptype]:
                        yield pwid, d['rpw'], d['tpw'], ''.join(x[0] for x in d['ttpw'])


def get_all_typed_words_zenbook():
    import csv
    csvf = csv.DictReader(open('all_typed_pass.txt'))
    return csvf


def get_all_typed_words(exptype=GENERAL):
    for row in pwresultstable.all():
        if row['exptype'] != exptype:
            continue
        rpw = row['rpw']#.strip()
        tpw = row['tpw']#.strip()
        ttpw = eval(row['ttpw'].strip())
        if not tpw or not ttpw: continue
        ttpw = ''.join(zip(*ttpw)[0]).strip()
        yield row['pwid'], rpw, tpw, ttpw


def get_all_typed_words_raw(exptype=GENERAL):
    for row in pwresultstable.all():
        if row['exptype'] != exptype:
            continue
        rpw = row['rpw']#.strip()
        tpw = row['tpw']#.strip()
        ttpw = eval(row['ttpw'].strip())
        if not tpw or not ttpw: continue
        # ttpw = ''.join(zip(*ttpw)[0]).strip()
        yield row['pwid'], rpw, tpw, ttpw

def compute_typingtime(ttpw):
    """
    returns the time taken to type the password in milisec
    """
    ttpw = eval(ttpw)
    if len(ttpw)>1:
        return (ttpw[-1][1] - ttpw[0][1])
    return 0


def typing_stats():
    for row in pwresultstable:
        if row['tpw']:
            yield row['pwid'], row['rpw'], row['tpw'], compute_typingtime(row['ttpw'])


def total_correct(exptype=None, popular_only=False):
    pwid_limit = "pwid000100000" if popular_only else "pwid000300000"
    
    num_corrects = db.query('SELECT COUNT(*) c from pwresults where rpw '\
                            'collate latin1_general_cs = tpw and pwid<="{}"'\
                            .format(pwid_limit)).next()['c']

    return num_corrects

incorrect_pwids = set(x.strip() for x in open('data/invalidpwids.txt'))

def get_typos(exptype=None, popular_only=True):
    # try:
    #     T = open_db_table()
    # except:
    #     print "Could not open the mysql typodata. Using the local \"all_typed_pass.txt\" file."
    #     T = get_all_typed_words_zenbook()

    """
    Only for GENERAL KEYBOARD NOW"""
    pwid_limit = "pwid000100000" if popular_only else "pwid000300000"    
    all_incorrect_entries = db.query("SELECT * from pwresults where tpw!='' and "\
                                     "rpw collate latin1_general_cs != tpw and pwid<='{}' order by pwid"\
                                     .format(pwid_limit))
    override_pws = {}
    for line in open('err_pws'):
            try:
                pwid, rpw, tpw = line.split(',',2)
            except:
                print "ERRRRRRRRRR", line
                exit(0)
            override_pws[pwid] = (rpw, tpw)

    for row in all_incorrect_entries:
        if row['pwid'] in override_pws:
            rpw, tpw = override_pws[row['pwid']]
            #print rpw, tpw
            if tpw != row['tpw']: continue
            #row['rpw'], row['tpw'] = rpw, tpw
        rpw = row['rpw'] = row['rpw'].strip()
        tpw = row['tpw'] = row['tpw'].strip()
        pwid = row['pwid']
        
        if row['pwid'] in incorrect_pwids:
            continue
        if (tpw and (not exptype or row['exptype']==exptype) and # the exptype matches
            (not popular_only or int(row['pwid'][4:])<1e5)): # only take the popular ones
            status = 'notsubmitted' if row['status'] != 'Approved' else 'approved'
            row['istypo'] = (tpw and  # filter 1, null types
                             rpw != tpw and  # not equal to the original value
                              # the typed version is not junk
                             lv.distance(rpw.lower(), tpw.lower()) <= TYPO_EDIT_DIST_LIMIT)
            if row['istypo']:
                yield row


def get_next_hit(count=1, char_lim=180, test=True):
    undone_pws = db.query("select pwid, rpw from pwresults where status='Available' order by rand()");
    
    while count > 0:
        count -= 1
        total_size = 0
        pws = {}
        while total_size<char_lim:
            try:
                r = undone_pws.next()
                pws[r['pwid']] = r['rpw']
                total_size += len(r['rpw'])
            except StopIteration:
                print "Left %d HITs, don't have any pwids left@!" % count
                count = 0
                break
        if not test:
            for pw in pws:
                db.query('update pwresults set status="Submitted" where pwid="{}"'.format(pw));
        yield pws


def get_next_hit_old(count=100, char_lim=180):
    """
    create @count hits pwids which are not done or submitted
    """
    import itertools
    base_dir = "/home/rahul/dsss/code/"     # the worst way to code, but I dont have time
    pwdataset = json.load(open('{base_dir}/mturk/pwset/S1_b4b37058387df'.format(base_dir=base_dir)))
    done_submitted = set(pwid for pwid,_,_,_ in get_all_typed_words(exptype=GENERAL))|\
                     set(l.strip() for l in open('{base_dir}/mturk/submitted_pwids.txt'.format(base_dir=base_dir)))
    print "Already done or submitted: {}".format(len(done_submitted))
    not_done = [pwid for pwid in pwdataset if pwid not in done_submitted]
    shuffle(not_done)

    # def _next_undone_pw():
    #     for pwid in not_done:
    #         yield pwid, pwdataset[pwid]

    next_undone_pw = iter(not_done)

    submitted_fl = open('../submitted_pwids.txt', 'a+')
    while count > 0:
        count -= 1
        total_size = 0
        pws = {}
        while total_size<char_lim:
            try:
                pwid = next_undone_pw.next()
                rpw = pwdataset[pwid]
                pws[pwid] = rpw
                total_size += len(rpw)
            except StopIteration:
                print "Left %d HITs, don't have any pwids left@!" % count
                count = 0
                break
        submitted_fl.write('\n'.join(pws.keys())+'\n')
        yield pws


def mysql2sqlite3(mysqldbname, sqlite3fname):
    with dataset.connect("myslq://localhost/%s" % mysqldbname) as mysqldb:
        for tables in mysqldb:
            pass


def typosmysql2json():
    pass

if __name__ == "__main__":
    for hit in get_next_hit(count=10):
        print hit
        
    exit(0)
    import csv
    with open("all_typed_pass.txt", 'wb') as f:
        csvf = csv.writer(f)
        csvf.writerow(["pwid", "rpw", "tpw"])
        csvf.writerows(row[:-1] for row in get_all_typed_words())


import json
from helper import open_, dp
import os
import Levenshtein as lv

thisdir = os.path.dirname(os.path.abspath(__file__))

typodata = {
    'mturk15-g': os.path.join(thisdir, 'mturk15-general.json.bz2'),
    'mturk15-t': os.path.join(thisdir, 'mturk15-touchonly.json.bz2'),
    'oakland12': os.path.join(thisdir, 'Oakland12passwords.json.bz2'), 
    'chi14': os.path.join(thisdir, 'CHI14longpasswords.json.bz2'),
}

def get_cmu_tpw_rpw(falias, recall=2):
    assert falias in ['oakland12', 'chi14'], "Cmudata does not have alias: {!r}".format(falias)

    for row in json.load(open_(typodata[falias])):
        r = row['recall%d_attempts'%recall]
        if not r: continue
        tpws = filter(lambda x: x, r) 
        assert isinstance(tpws, list), tpws
        yield row['password'], tpws


def get_mturk_tpw_rpw(falias='mturk15-g', **kwargs):
    assert falias.startswith('mturk15'), "Mturk data does not have alias: {!r}".format(falias)
    for row in json.load(open_(typodata[falias])):
        yield row['rpw'], [row['tpw']]

def get_rpw_tpw(falias, **kwargs):
    return {
        'mturk15-t': get_mturk_tpw_rpw,
        'mturk15-g': get_mturk_tpw_rpw,
        'oakland12': get_cmu_tpw_rpw,
        'chi14': get_cmu_tpw_rpw
        }[falias](falias, **kwargs)

# def fix_cmu_data():
#     for f in ['oakland12', 'chi14']:
#         D = []
#         for row in json.load(open_(typodata[f])):
#             r1, r2 = row['recall1_attempts'], row['recall2_attempts']
#             if not isinstance(r1, list):
#                 row['recall1_attempts'] = [r1]
#             if not isinstance(r2, list):
#                 row['recall2_attempts'] = [r2]
#             D.append(row)
#         with open_(typodata[f], 'w') as outf:
#             json.dump(D, outf, indent=2)


def measure_utility(checker):
    #  print "CMU data stats!"
    # print "Fraction of password corrected <-> fraction of remaing typo. (c/t <--> f/t)"
    c, f, l = 0, 0, []
    meme = 0
    sl, fl, tl = 0, 0, 0 # successful login, failed login, typoed login
    recall = 1
    dataaliases= [('oakland12', 1), ('oakland12', 2),
                ('chi14', 1), ('chi14', 2),
                ('mturk15-t', 0), ('mturk15-g', 0)]
    for dt in dataaliases[-1:]:
        print "data={}\trecall={}".format(*dt)
        for rpw, tpws in get_rpw_tpw(falias=dt[0], recall=dt[1]):
            l.append(len(tpws))
            for tpw in tpws:
                if tpw != rpw:
                    f += 1
                    s = "{!r} <--> {!r}"
                    tpw = tpw.encode('ascii', errors='replace')
                    rpw = rpw.encode('ascii', errors='replace')
                    if checker.check(tpw, rpw):
                        s += "\t\t(c)"
                        c += 1
                    elif lv.distance(tpw.lower(), rpw.lower())>3:
                        meme += 1
                        # print s.format(rpw, tpw)
        t = sum(l)
        dp(c=c, f=f, t=t, meme=meme, l=len(l))
        print "c/t: {:5.2f}, f/t: {:5.2f}".format(float(c)/t, float(f)/t)
    

    

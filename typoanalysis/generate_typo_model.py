#!/bin/python
from collections import defaultdict
import re
import string
from math import log
import csv
import Levenshtein as lv
import numpy as np
# from edits.keyboard import Keyboard
from word2keypress import Keyboard
#from config import (ALLOWED_KEYS, CAPS_KEY, SHIFT_KEY,
#                         STARTSTR, ENDSTR, BLANK, ALLOWED_CHARACTERS)
SHIFT_KEY = chr(3) # [u'\x03', "<s>"][user_friendly]
CAPS_KEY = chr(4) # [u'\x04', "<c>"][user_friendly]
STARTSTR = chr(1)
ENDSTR = chr(2)
BLANK = chr(5)

ALLOWED_KEYS = b"`1234567890-=qwertyuiop[]\\asdfghjkl;'zxcvbnm,./ {}{}"\
    .format(SHIFT_KEY, CAPS_KEY)
ALLOWED_CHARACTERS = string.printable[:-5]

from . import edits.weighted_edist as wedist

KB = Keyboard('qwerty')

def get_typos(fname):
    for row in csv.reader(open(fname)):
        yield row

def process_theword(word):
    """
    process the word if there is '\b' etc.
    """
    return re.sub(r'\t\b', '', word)


def align(word1, word2):
    """
    :param word1: string
    :param word2: string
    :return: (list, list)
    Aligns two strings using Levenshtein distance and returns two aligned lists.
    """
    word1 = process_theword(word1).decode('utf-8')
    word2 = process_theword(word2).decode('utf-8')

    edits = lv.editops(word1.lower(), word2.lower())
    A = list(word1)
    B = list(word2)

    for e in edits:
        ename, s, d = e
        if ename == 'insert':
            A.insert(s, BLANK)
        elif ename == 'delete':
            B.insert(s, BLANK)
    A.insert(0, STARTSTR)
    A.append(ENDSTR)
    B.insert(0, STARTSTR)
    B.append(ENDSTR)

    return A, B


def aligned_stringlets(word1, word2, k=1, only_mistakes=False):
    """
    returns the list of pair of substrings of size 2*k+1 (k past, k future and itself).
    :param word1: string
    :param word2: string
    :param k: integer
    :return: list of tuples
    """
    A, B = wedist.align(word1, word2)
    assert len(A) == len(B), "WTH! length of A ({}) and B ({}) are not equal.".format(len(A), len(B))
    l = k   # 2*k + 1
    for i in range(0, len(A)-l):
        a, b = ''.join(A[i:i+l]), ''.join(B[i:i+l])
        if not only_mistakes or a != b:
            yield a, b


def get_all_typolets(k=1, iteration=0, fname=''):
    A = defaultdict(int)
    total_typos = 0
    for row in get_typos(fname):
        total_typos += 1
        # arpw, atpw = align(row['rpw'], row['tpw'])
        # print "{!r}\t{!r}\n{!r}\t{!r}\n\n".format(''.join(arpw), row['rpw'],
        #                                           ''.join(atpw), row['tpw'])
        # continue
        for pairs in aligned_stringlets(row['rpw'], row['tpw'],
                                        k=k, only_mistakes=False):
            A[pairs] += 1
    if iteration > 0:
        wedist.update_weight_matrix(A)
        get_all_typolets(k=k, iteration=iteration-1)
    for x in list(A.keys()):
        if x[0] == x[1]:
            del A[x]
    print("Total Typos processed:", total_typos)
    print('\n'.join("{!r},{!r},{}".format(x[0][0], x[0][1], x[1])
                    for x in sorted(list(A.items()), key=lambda x: x[1], reverse=True)))


def create_heat_map(exptype="GENERAL"):
    import json, os
    JUNK = '\x07'
    allowed_characters = ALLOWED_KEYS[:-2]
    print(len(allowed_characters))

    if os.path.exists('data/heatmap.csv'):
        heatmap = list(csv.reader(open('data/heatmap.csv')))
    else:
        char2id = dict((c, i) for i, c in enumerate(allowed_characters))

        heatmap = [list('char'+allowed_characters)] + \
                  [[a] + [0]*len(allowed_characters) 
                   for i, a in enumerate(allowed_characters)]

        for row in get_typos():
            rpw, tpw = row['rpw'], row['tpw']
            A, B = wedist.align(rpw, tpw)
            for a, b in zip(A, B):
                try:
                    heatmap[char2id[a]+1][char2id[b]+1] += 1
                except KeyError:
                    print("Could not find a={!r} or b={!r}".format(a,b))
                    continue
                except IndexError:
                    print("Could not find Index:", a, b, char2id[a], char2id[b])
                    exit(0)
        #for row in T:
        #    # s = log(sum(row))
        #    for i, c in enumerate(row):
        #        row[i] = log(row[i])  # - s

        # json.dump(T, open('heatmap.json', 'w'), indent=2)
        csvwriter = csv.writer(open('data/heatmap.csv', 'w'))
        #csvwriter.writerow(allowed_characters)
        csvwriter.writerows(heatmap)

    create_heatmap_matplotlib(heatmap, allowed_characters, allowed_characters)


def create_heatmap_latex(T, Xlabel, Ylabel):
    import string

    def latex_safe(c):
        return {'#': '\#',
                '$': '\$',
                '^': '$\\hat{}$',
                '}': '\}',
                '{': '\{',
                '%': '\%',
                '&': '\&',
                '_': '\_',
                '\\': "$\\textbackslash$",
                '~': "$\texttilde$",
                '\x00': '[del]',
                '\x01': '[start]',
                '\x02': '[end]',
                '\x07': '[err]',
                }.get(c, c)

    # S = "\\node {nodevalue}; & \\HeatmapNode{-300} & \\HeatmapNode{-200} & \\HeatmapNode{-100} \\\\"
    row_header = "\\node {};  & " + '& '.join('\\node{%s}; ' % latex_safe(x) for x in Xlabel)
    print(len(T), len(Ylabel), len(Xlabel))
    rows = '\\\\\n'.join("\\node{%s}; & " % latex_safe(Ylabel[i]) + '& '.join('\HeatmapNode{%d} ' % (v*10) for v in T[i])
                     for i in range(len(T)))
    T = string.Template(open('../graph/heatmap/heatmap.tex').read()).safe_substitute(rowheader=row_header, rows=rows)
    open('../graph/heatmap/heatmap1.tex', 'wb').write(T)


    
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.cm as cm
import matplotlib.colors as mcolors

def colorbar_index(ncolors, cmap):
    cmap = cmap_discretize(cmap, ncolors)
    mappable = cm.ScalarMappable(cmap=cmap)
    mappable.set_array([])
    mappable.set_clim(-0.5, ncolors+0.5)
    colorbar = plt.colorbar(mappable)
    colorbar.set_ticks(np.linspace(0, ncolors, ncolors))
    colorbar.set_ticklabels(list(range(ncolors)))

def cmap_discretize(cmap, N):
    """Return a discrete colormap from the continuous colormap cmap.

        cmap: colormap instance, eg. cm.jet. 
        N: number of colors.

    Example
        x = resize(arange(100), (5,100))
        djet = cmap_discretize(cm.jet, 5)
        imshow(x, cmap=djet)
    """

    if type(cmap) == str:
        cmap = plt.get_cmap(cmap)
    colors_i = np.concatenate((np.linspace(0, 1., N), (0.,0.,0.,0.)))
    colors_rgba = cmap(colors_i)
    indices = np.linspace(0, 1., N+1)
    cdict = {}
    for ki,key in enumerate(('red','green','blue')):
        cdict[key] = [ (indices[i], colors_rgba[i-1,ki], colors_rgba[i,ki])
                       for i in range(N+1) ]
    # Return colormap object.
    return mcolors.LinearSegmentedColormap(cmap.name + "_%d"%N, cdict, 1024)

def create_heatmap_matplotlib(T, Xlabel, Ylabel):
    import matplotlib.pyplot as plt
    import numpy as np
    from matplotlib.backends.backend_pdf import PdfPages
    from matplotlib import rc
    import pylab 
    rc('font',**{'family':'sans-serif', 'sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    #rc('font',**{'family':'serif','serif':['Palatino']})
    # rc('text', usetex=True)


    pp = PdfPages('heatmap.pdf')
    T = [[int(x) for x in t[1:]] for t in T[1:]]
    for i,t in enumerate(T):
        T[i][i] = 0

    column_labels = [c if i % 3 == 0 else c for i, c in enumerate(Xlabel)]
    row_labels = [c if i % 3 == 0 else c for i, c in enumerate(Ylabel)]
    i = Xlabel.index(BLANK)
    column_labels[i] = '[ins]'
    row_labels[i] = '[del]'
    for k,v in list({CAPS_KEY:'$<$c$>$', 
                SHIFT_KEY: '$<$s$>$',
                STARTSTR: '[start]',
                ENDSTR: '[end]',
                '`': '$\`{}$',
                ' ': " " 
    }.items()):
        ci = Xlabel.find(k)
        row_labels[ci] = v
        column_labels[ci] = v

    data = np.array([[log(x+1) for x in row] for row in T])

    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111, autoscale_on=False)
    cmap = plt.get_cmap('YlGnBu')
    heatmap = ax.pcolor(data, cmap=cmap) #cmap='RdBu_r')
    # heatmap = pylab.imshow(data)
    # colorbar_index(ncolors=len(T), cmap=cmap)

    # for k,v in ax.spines.items():
    #     v.set_visible(False)
        # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('top')
    # put the major ticks at the middle of each cell
    xticks = np.arange(data.shape[0]+1)+0.5
    xticks[-1] = data.shape[0]
    yticks = np.arange(data.shape[1]+1)+0.5
    yticks[-1] = data.shape[1]

    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    
    # want a more natural, table-like display
    ax.invert_yaxis()

    ax.set_xticklabels(row_labels, rotation=90, fontsize=13)
    ax.set_yticklabels(column_labels, fontsize=13)
    # fig.tight_layout()
    ax.set_xlabel("$c'$", fontsize=24)
    ax.set_ylabel("$c$", fontsize=24)
    ax.xaxis.set_label_position('top')

    #plt.savefig(pp, format='pdf')
    #pp.close()
    plt.savefig('heatmap.pdf', bbox_inches='tight')
    #plt.show()

if __name__ == "__main__":
    ## for str_pairs in [('abcd', 'asdb'), ('asdfasdf', 'dfasdf')]:
    #   print '\n'.join(str(x) for x in aligned_stringlets(*str_pairs))
    get_all_typolets(k=0, iteration=1, fname=sys.argv[1])
    #  create_heat_map()
    exit(0)

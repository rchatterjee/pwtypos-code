__author__ = "rahul"
import os, sys, csv, json
import re, random
import Levenshtein as lv
from collections import defaultdict
import unittest, helper
from .keyboard import Keyboard
# from typodata.dbaccess import get_typos, db, pwresults_data
from security.config import *
from .edits import Edits
from Levenshtein import distance as edist


"""
What are the things I am going to record
  a) typo location (beginning, end, middle)
  b) Capitalization, All wrong case
  c) deletion, insertion, substitution/replace
  d)
"""
# ALLOWED_EDITS = ['same', 'swc-all', 'swc-first', 'rm-lastc',
#                  'rm-firstc', 'n2s-last',
#                  'upncap', 'sws-last1', 'add1-last']
ALLOWED_EDITS = ['same', 'swc-all', 'swc-first', 'rm-lastc',
                 'rm-firstc', 'upncap', 'sws-lastn', 'add1-last']
added_edits = ['proximity', 'iofix', 'memerror', 'other', 'tcerror']

MOBILE = "MOBILE"
GENERAL = "GENERAL"


def edits2typo(word1, word2):
    word1, word2 = word1.replace('\b', ''), word2.replace('\b', '')
    edits = lv.editops(word1, word2)
    M = []
    medits = {}
    # 'insert': defaultdict(str),
    #          'delete': defaultdict(str),
    #          'replace': []}
    # merge edits
    for e in edits:
        ename, s, d = e
        if ename is 'insert':
            v = medits['insert'] = medits.get('insert', {})
            v[s] = v.get(s, '') + word2[d]
        elif ename is 'delete':
            v = medits['delete'] = medits.get('insert', {})
            v[d] = v.get(d, '') + word1[s]
        elif ename is 'replace':
            v = medits['replace'] = medits.get('replace', [])
            v.append((word1[s], word2[d]))
    return dict((k, list(v.values())) if k != 'replace' else (k, v) for k, v in list(medits.items()))


most_probable_key_prox_error = defaultdict(int)
def keyboard_close_replace(word1, word2):
    global most_probable_key_prox_error
    # kb = Keyboard('MOBILE_ANDROID', shift_discount=3)
    kb = Keyboard('US', shift_discount=2.0)
    cnt = 0
    onlyio = 0
    check = ['i', 'o']
    if not (kb.is_all_typable(word1) and kb.is_all_typable(word2)):
        return -1

    for r in edits2typo(word1, word2).get('replace', []):
        d = kb.keyboard_dist(*r)
        if d<3:
            if r[0]>r[1]:
                r = (r[1], r[0])
            if r[0] in check and r[1] in check:
                onlyio+=1
            most_probable_key_prox_error[r] += 1
            cnt += 1
    # if cnt==1 and onlyio>0:
    #     cnt = -1
    # elif onlyio>1:
    #     print "-->", onlyio, cnt, word1, word2
    return cnt;

def transcription_error_check(rpw, tpw):
    confusing_chars = dict((x,a[0]) for a in
                           [['1', 'l', 'I', 'i'],
                            ['o', '0', 'O'],]
                           for x in a)
    nrpw = ''.join(confusing_chars.get(c,c) for c in rpw)
    ntpw = ''.join(confusing_chars.get(c,c) for c in tpw)
    return nrpw == ntpw

def slow_levenshtein(s_w, t_w):
    """
    s_w: source word
    d_w: typed word
    returns edits, as he types
    """
    i, j = 0, 0
    while i < len(s_w) and j < len(t_w):
        if s_w[i] != t_w[j]:
            pass        

# First basic analysis
def collect_typo_stat_on_length(exptype=GENERAL):
    """open the [gid]_data.csv files and calculate how many mistakse are there
    csv file: 
        pwid,caps_lock,rpw,ttpw,tpw
    """
    Ldr, Ldt = defaultdict(int), defaultdict(int)  # length effect collector
    key_close_error_cnt = 0
    total = 0
    dbTable = open_db_table()
    for row in dbTable.find(exptype=exptype):
        rpw, tpw = row['rpw'], row['tpw']
        ttpw = ''.join(x[0] for x in json.loads(row['ttpw']))
        # if row['pwid'] > 'pwid000100000': continue # only popular password
        # Length effect
        # Ldr[len(rpw)] += 1
        if rpw != tpw.strip():
            total += 1
            # Ldt[len(rpw)] += 1
            print("{!r} <--> {!r}".format(rpw, tpw))  # , edits2typo(unicode(rpw),unicode(tpw))
            # print row['caps_lock']
            key_close_error_cnt += 1

    print("Total: {}\tKey_close_error: {}".format(total, key_close_error_cnt))
    for k, v in list(Ldr.items()):
        print(k, v, Ldt[k], float(Ldt[k]) / v)
    print(sum(Ldr.values()), sum(Ldt.values()))


def collect_typo_stat_on_composition(fname):
    """open the [gid]_data.csv files and calculate how many mistakse are there
    csv file: 
        pwid,caps_lock,rpw,ttpw,tpw
    """
    csvfile = csv.DictReader(open(fname))
    Ldr, Ldt = defaultdict(int), defaultdict(int)  # length effect collector
    # Password policies
    # 1. at least 6 character long -- basic6
    # 2. at least 8 character long -- basic8
    # 3. at least 8 character long and has 4 character classes -- comp8c4
    # 4. at least 8 character long and has at least 3 character classes -- comp8c3
    # 5. at least 6 character long and having a symbol or digit. -- mod6ds
    # 6. at least 6 character long and having lower-case or upper-case and digit or symbol -- mod6cc2

    def add_to(c, ismistake):
        Ldr[c] += 1
        if ismistake:
            Ldt[c] += 1

    for row in csvfile:
        # if row['pwid'] > 'pwid000100000': continue # only popular password
        rpw, tpw = row['rpw'], row['tpw']
        n = len(rpw)
        mistake = (rpw != tpw)
        if n >= 6:  # basic6
            add_to('basic6', mistake)
        if n >= 8:
            add_to('basic8', mistake)
        C = helper.what_classes(rpw)
        if len(C) == 4 and n >= 8:
            add_to('comp8c4', mistake)
        if len(C) == 3 and n >= 8:
            add_to('comp8c3', mistake)
        if 'D' in C and 'S' in C and n >= 6:
            add_to('mod6ds', mistake)
        if ('U' in C or 'L' in C) and ('D' in C or 'S' in C) and n >= 8:
            add_to('mod6cc2', mistake)

    for k, v in list(Ldr.items()):
        print("{},{},{},{}".format(k, v, Ldt.get(k, 0),
                                   Ldt.get(k, 0) / float(v)))


def count_typo_classes(rpw, tpw, editsd):
    """Takes a real password (@rpw) and the typoed version (@tpw), and
    updates the (@editsd) dictionary with the counts.
    """
    transforms = Edits()
    fixed = False
    te = ''
    for e in ALLOWED_EDITS:
        f = transforms.EDITS_NAME_FUNC_MAP[e][0]
        if f(tpw) == rpw:
            if fixed:
                print("Mutations are not exclusive!!!!, tpw={tpw}, e={e}, rpw={rpw}"\
                    .format(e=e, rpw=rpw, tpw=tpw))
            editsd[e] += 1
            te = e
            fixed = True
    if not fixed:
        kd = keyboard_close_replace(rpw.lower(), tpw.lower()) 
        if kd==1:
            editsd['proximity'] += 1
        # elif kd==-1:
        #     editsd['iofix'] += 1
        elif transcription_error_check(rpw, tpw):
            # print rpw, tpw
            editsd['tcerror'] += 1
        # If Jaro-Winkler simialrity is too low, then most likely this
        # is a memory error
        elif lv.jaro_winkler(str(rpw.lower()), str(tpw.lower()))<0.85:
            editsd['memerror'] += 1
        else:
            editsd['other'] += 1
            
    return te


def typo_fix_stat():
    from typodata.dbaccess import get_typos, db, pwresults_data
    editsd = {e: 0 for e in ALLOWED_EDITS}
    editsd.update({
        'proximity': 0, 'other': 0, 'iofix': 0, 'tcerror': 0, 'memerror': 0
    })
    
    # Load the  wrong caps lock errors            
    total_samples = 0
    error = 0 
    capslock_error_groups = [0,0,0]
    capslock_total = [0,0,0]
    for row in db.query("select rpw, tpw from typedresultsmobile where " 
                        "tpw!= '' and rpw collate latin1_general_cs != tpw"):
    # for row in get_typos(): # pwresults_data(type_='popular'):
        if edist(row['tpw'].strip().lower(), row['rpw'].strip().lower())>5:
            continue
        k = int(row.get('capslock', 0))
        # if k<20: continue
        if k>=0:
            capslock_total[k/10] += 1
        total_samples += 1
        row['istypo'] = (row['rpw'] != row['tpw'])
        if row['istypo']:
            # if random.random()<0.01:
            #     print "{!r}<-->{!r}".format(row['rpw'], row['tpw'])
            e = count_typo_classes(row['rpw'], row['tpw'], editsd)
            if e =='swc-all' and k>0:
                capslock_error_groups[k/10] += 1
                # print rpw, tpw, row['capslock']

    total_errors = sum(editsd.values())
    for k,v in list(editsd.items()):
        print("{}: {:.3f}%".format(k, v*100.0/total_errors))
    print('\n\n')
    print(json.dumps(editsd, indent=2))
    print("Total Errors = ", total_errors)
    print("Total Samples:", total_samples)
    print("Capslock error groups:", capslock_error_groups)
    print("Capslock total:", capslock_total)
    for k,v in list(editsd.items()):
        editsd[k] = v/float(total_errors)

    # print sorted(most_probable_key_prox_error.items(), key=operator.itemgetter(1))[-20:]
    return editsd


# Effect of caps-lock
def collect_typo_stat_on_capslock(fname):
    """open the [gid]_data.csv files and calculate how many mistakse are there
    csv file: 
        pwid,caps_lock,rpw,ttpw,tpw
    """
    csvfile = csv.DictReader(open(fname))
    Ldr, Ldt = defaultdict(int), defaultdict(int)  # length effect collector
    for row in csvfile:
        rpw, tpw = row['rpw'], row['tpw']
        if not re.search(r'[A-Z]', rpw): continue  # no alphabet
        n = len(rpw)
        mistake = (rpw != tpw)
        cl = int(row['caps_lock'])
        Ldr[0 if cl < 10 else 1] += 1
        if mistake:
            Ldt[0 if cl < 10 else 1] += 1
    for k, v in list(Ldr.items()):
        print("{},{},{},{}".format(k, v, Ldt.get(k, 0),
                                   Ldt.get(k, 0) / float(v)))



def test_tab_key():
    """
    Test whether accidental pressing tab key is an issue or not.
    """
    for row in db.query("select rpw, tpw, ttpw from typedresultsgeneral where " \
                              "tpw!= ''"):
        ttpw = list(zip(*json.loads(row['ttpw'])))
        if ttpw:
            ttpw = ttpw[0]
            ttpw = ''.join(ttpw).strip('\t')

        t = 0
        if '\t' in ttpw:
            t = ttpw.index('\t')
        if t>0 and t<len(ttpw)-1:
            print(repr(''.join(ttpw)), t, row['tpw'], row['rpw'])


################################################################################
## CMU data Analysis
################################################################################
attempt = 1
print("## recall{}-Attempt".format(attempt))
def test_cmu_data_typos():
    """
    Test CMU data given by Lujo
    """
    editsd = defaultdict(int)
                                                   
    # Load the  wrong caps lock errors            
    total_samples, total_errors = 0, 0
    error = 0 
    capslock_error_groups = [0,0,0]
    capslock_total = [0,0,0]

    from cmudata.parse import get_cmu_tpw_rpw

    for rpw, tpw in get_cmu_tpw_rpw(attempt=attempt):
        total_samples += 1
        if rpw != tpw:
            total_errors += 1
            _ = count_typo_classes(rpw, tpw, editsd)
    for k,v in list(editsd.items()):
        editsd[k] = v*100.0/total_errors

    print('\n'.join("|%20s | %5.2f |" % (k,v)
              for k,v in list(editsd.items())))

    print('\n\n')
    # print json.dumps(editsd, indent=2)
    print("Total Errors = ", total_errors)
    print("Total Samples: {} ({:.2f} %)".format(total_samples, total_errors * 100.0 / total_samples))

def test_cmu_data_failed_logins():
    transforms = Edits()
    def can_it_be_fixed(rpw, tpws):
        for i, tpw in enumerate(tpws):
            for e in ALLOWED_EDITS[:5]:
                f = transforms.EDITS_NAME_FUNC_MAP[e][0]
                if f(tpw) == rpw:
                    return i
        return -1
    from cmudata.parse import failed_logins, get_cmu_loginattempts
    total_failed_login = 0
    fixable_failed_login = 0
    total_pw_submission = 0
    total_login = 0
    pw_submission_saved = 0
    # for rpw, tpws in failed_logins():
    for rpw, tpws in get_cmu_loginattempts(attempt=attempt):
        total_pw_submission += len(tpws)
        total_login += 1
        i_fixable_tpw = can_it_be_fixed(rpw, tpws) 
        if i_fixable_tpw>=0:
            pw_submission_saved += (len(tpws) - i_fixable_tpw - 1)

        if rpw != tpws[-1]:
            total_failed_login += 1
            if i_fixable_tpw >=0 :
                fixable_failed_login += 1

    wrong_submission  = total_pw_submission - total_login + total_failed_login
    print('\n'.join( "|%25s | %8.2f |" % (k,v)
               for k, v in [("Num Login Attempts", total_login),
                            ("Failed logins", total_failed_login*100.0/total_login),
                            ("% Fixable", 100.0*fixable_failed_login/total_failed_login),
                            ("Num pw submissions", total_pw_submission),
                            ("% Wrong submissions", wrong_submission*100.0/total_pw_submission),
                            ("% submissions be saved", 100.0 * pw_submission_saved/ (total_pw_submission-total_login))]))

        
class TestTypoTypes(unittest.TestCase):
    def test_edits2tyops(self):
        inp_res_map = [
            (('titittitit', 'titit'), ({'insert': ['titit']})),
            (('louie', ';puie'), ({'replace': [('l', ';'), ('o', 'p')]}))
        ]

        for q, r in inp_res_map:
            self.assertEqual(edits2typo(*q), r,
                             "%s --> %s" % (q, r))


if __name__ == '__main__':
    # unittest.main()
    if sys.argv[1] == '-func':
        try:
            print(eval("%s(%s)" % (sys.argv[2], ','.join(repr(x) for x in sys.argv[3:]))))
        except NameError:
            print("Could not find the function: {}".format(sys.argv[2]))
    else:
        exptype = GENERAL if sys.argv[1].upper() == GENERAL \
            else MOBILE if sys.argv[1].upper() == MOBILE else None

        collect_typo_stat_on_length(exptype)
        # collect_typo_stat_on_composition(sys.argv[1])
        # collect_typo_stat_on_capslock(sys.argv[1])

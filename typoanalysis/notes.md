## Estimate largest ball size
* typo of pw is a string that is within edit distance 1 from the original password pw.
* Here ball of typo w~ (B(w~)) is a set of passowrds each of which is mistyped to w~. 
* T(w, w~) is the probability that a w is mistyped to w~.
* Edge-weight of a ball b(w~) = \sum_w T(w, w~)

Goal: Estimate the number of balls for which b(w~)>1. Or, max_{w~} b(w~)
And, finally sum{w~, b(w~)>1} b(w~)?

Find the largest ball in the distribution.  If the size of the ball is
k, and there are total n passwords, the probability that that two
randomly chosen passwords lies in the same ball is $(k x (k-1))/(n x
(n-1))$

Lets put some number in it.  k = 100, n = 1M, p = 10^4/10^12 = 10^-8,
now if we make 10^4 guesses then there 50% chance (by birthday bound)
that we will see at least 1 pair that belong to the clique. (USELESS)


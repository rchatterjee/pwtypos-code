
import numpy as np
import sys, os

sys.path.append(os.path.join(os.path.expanduser('~'), 'passwords/'))
from readpw import Passwords
from pyxdameraulevenshtein import damerau_levenshtein_distance as dldist
from pyxdameraulevenshtein import \
    damerau_levenshtein_distance_ndarray as dldist_ndarray
import multiprocessing as mp
from multiprocessing import Pool
import networkx as nx
from collections import defaultdict
from word2keypress.typos import get_topk_typos
import json

"""
TODO:
1. convvert the passwords into key-press sequence before calling dldist.
2.
"""

"""
damerau_levenshtein_distance_ndarray can compute roughly 90 ms per word
for 100k size array
"""
LIMIT = int(1e6)
EDIST_CUTOFF = 2

dataids, datapws = '', ''
datapw_len = np.array([])
X = []


def get_passwords(fname='/home/rahul/passwords/rockyou-withcount.txt.bz2'):
    global datapws, dataids, datapw_len, X
    pws = Passwords(fname, min_pass_len=6)
    pwiter = pws.justiter()
    print(len(pws))
    l = min(LIMIT, len(pws))
    a0, a1, a2 = list(zip(*[next(pwiter) for _ in range(l)]))
    datapws = np.array(a1)
    dataids = np.array(a0)
    datafs = np.array(a2)
    datapw_len = np.vectorize(len, otypes=[np.uint32])(datapws)
    X = np.arange(datapws.shape[0])
    return pws, (datapws, dataids, datafs)


# def dldist_ndarray(w):
#     np.array([dldist(w, s)<=EDIST_CUTOFF for s in datapws])

def within_dist(i):
    w = datapws[i]
    f_ = (np.abs(datapw_len - len(w)) <= EDIST_CUTOFF)
    if (i % 1000 == 1):
        print("Done {}".format(i - 1))
    return ((X[f_])[i+1:])[dldist_ndarray(w, datapws[f_][i+1:]) <= EDIST_CUTOFF]


def call_(s, e):
    print("--->", s, e)
    return np.array([
        within_dist(i) for i in range(s, e)
    ], dtype=object)


def compute_distances(datapws, dataids):
    n_cpu = 6
    chunksize = int(datapws.shape[0] / n_cpu)
    print("Chunksize: {}".format(chunksize))
    res = []
    with  Pool(n_cpu) as pool:
        res = np.array(pool.map(within_dist, np.arange(datapws.shape[0]),
                                chunksize=chunksize))
        # return np.array(list(map(within_dist, datapws)), dtype=object)
    # Fill the other edges in other direction, that is
    # if (s,e) in res, then (e, s) should also be in res
    d = {i: [] for i in range(datapws.shape[0])}
    for i, a in enumerate(res):
        for j in a:
            d[j].append(i)
    for i, a  in enumerate(res):
        res[i] = np.concatenate([a, d[i]])
    return res


MIN_CLIQUE_SIZE = 10  ## ignore all cliques less than 10


def get_max_cliques(ofname):
    """
    Read the adjlist file created by compute_distance, and find the cliques.
    """
    d = np.load(ofname)
    elist = d['e2lists']
    pwids = d['pwids']
    elistlen = np.vectorize(len)(elist)
    min_clique, max_clique = MIN_CLIQUE_SIZE, np.max(elistlen)
    # Remove the nodes whose degree is less than min_clique_size
    nodes = np.where(elistlen > min_clique)[0]
    edges = elist[nodes]
    G = nx.Graph()
    print("Considering {} many nodes".format(nodes.shape))
    G.add_edges_from((v, x) for v, arr in zip(nodes, edges) for x in arr)
    max_cliques = [
        pwids[clids]
        for clids in nx.find_cliques(G) if len(clids) > MIN_CLIQUE_SIZE
    ]
    return np.array(max_cliques, dtype=object)


def compute_ball_edge_weight(pws, fname):
    """Compute the edge-wieght of a each ball. Where edgeweight is
    defined as the sum of probabilities of typos.  Not all cliques are
    balls.  Get top-1000 typos of each password in the clique, and
    then find the typo with maximum degree.
    """
    k_typos = 1000
    cliques = np.load(fname)['cliques']
    useful_pws = set(np.concatenate(cliques))
    print(list(map(pws.id2pw, useful_pws)))
    typos = defaultdict(float)
    for pwid in useful_pws:
        pw = pws.id2pw(pwid)
        for tpw, w in get_topk_typos(pw, k_typos):
            typos[tpw] += w
    return sorted(list(typos.items()), key=lambda x: x[1], reverse=True)[:100]


def run_(fname, ofname, stage=(0, 1, 2, 3)):
    if not stage:
        return run_(fname, ofname, stage=list(range(4)))
    todo = set(stage)
    ofcliques = ofname + '_cliques'
    ofweights = ofname + '_ball_w.json'
    datapws, dataids, pws = np.array([]), np.array([]), np.array([])
    if 0 in todo:
        print("Stage 0: Getting passwords "),
        pws, (datapws, dataids, datafs) = get_passwords(fname)
        print("...Done")
        todo.remove(0)
    if 1 in todo:
        print("Stage 1: Computing distances ", )
        ofdata = compute_distances(datapws, dataids)
        np.savez_compressed(ofname, e2lists=ofdata, pwids=dataids)
        print("\n> FOR DEBUGGING <")
        for x in ofdata[[20, 34, 56]]:
            print([pws.id2pw(dataids[y]) for y in x])
        print("> END <\n\n")
        print("...Done")
        todo.remove(1)
    if 2 in todo:
        print("Stage 2: Getting cliques from the edge adj list ", )
        np.savez_compressed(
            ofcliques,
            cliques=get_max_cliques(ofname + '.npz')
        )
        print("...Done")
        todo.remove(2)
    if 3 in todo:
        if 0 not in stage:
            todo.add(0)
            return run_(fname, ofname, todo)
        print("Stage 3: Computing the ball-weights ", )
        with open(ofweights, 'w') as f:
            json.dump(compute_ball_edge_weight(pws, ofcliques + '.npz'),
                      f, indent=4)
        print("...Done")
        todo.remove(3)
    print("Done all stages: {}\t todo={}".format(stage, todo))


if __name__ == '__main__':
    # mp.set_start_method('spawn')
    # print("Start method: ", mp.get_start_method())

    fname = sys.argv[1]
    ofname = sys.argv[2]
    stage = [int(x) for x in sys.argv[3:]]
    run_(fname, ofname, stage=stage)

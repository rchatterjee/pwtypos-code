import sys, os
# quickscripts.fix_user_agent()
import argparse



# parser=argparse.ArguemtParser(description="""
# This is a front end of the analysis tools for fixing password typos.
# Two main part of it -utility and -security.
# """)


func, arguments = sys.argv[1], sys.argv[2:]

if func in ['-util', '-utility']:
    from typodata import measure_utility
    from typofixer.checker import BUILT_IN_CHECKERS
    # chk = BUILT_IN_CHECKERS['ChkAll_keyedit']
    chk = BUILT_IN_CHECKERS['ChkSelect_keyedit']
    measure_utility(chk)

elif func=='-authserver':
    from security.pwmodel import PWModel
    from edits.authserver import get_qth_most_prob_pw
    pwm = PWModel(leakname='rockyou', evaluation_model=True)
    print(get_qth_most_prob_pw(pwm, 1000))

elif func=='-heatmap':
    from typoanalysis import generate_typo_model
    generate_typo_model.create_heat_map()

elif func == '-rand':
    from edits.authserver import AuthServerRandomized
    allowed_edits = ['same', 'swc-all', 'swc-first', 'rm-lastc',
                'rm-firstc', 'sws-last1', 'upncap',
                'add1-last']
    #pwmodel = PWModel(leakname='rockyou', pw_filter=config.PW_FILTER)
    aserver = AuthServerRandomized(allowed_edits, policy_num=1, little_k = 200, pwmodel=None)
    print(aserver.typo_fix_rate());
    
else:
    import importlib
    # import typodata, plots, security, edits
    # from typodata import dbaccess
    module = sys.argv[1].replace('/','.').replace('.py', '')
    func = sys.argv[2]
    A = getattr(__import__(module, 
                            fromlist=[func]), 
                func)
    #A()
    eval('A(*{})'.format(sys.argv[3:]))
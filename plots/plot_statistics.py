import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from itertools import cycle
from typodata.dbaccess import (db, hittable, pwresultstable, 
                               get_typos, pwresults_data)
from collections import defaultdict
import Levenshtein as lv
import operator, re
import json

plt.rc('font', family='serif', size=14)
from Levenshtein import distance as DLdist
markers = cycle([u'bo-', u'gv-', u'y^-', u'rs-', u'c>-', u'm8-', u's-', 
                 u'p-.', u'*-.', u'h-.', u'H-', u'D-', u'd-'])
def get_all_mistyped():
    return db.query("select * from pwresults where tpw!='' and rpw collate latin1_general_cs != tpw")
TYPO_EDIT_DIST_LIMIT = 5    


def get_ttime(ttpw):
    try:
        ttpw = eval(ttpw)
    except SyntaxError:
        print "SyntaxError:", ttpw

    if len(ttpw)>5:
        # return len(ttpw)*1000.0/(ttpw[-1][1] - ttpw[0][1])
        return (ttpw[-1][1] - ttpw[0][1])/1000.0
    return 0


def istypo(rpw, tpw):
    return  (tpw and
             rpw != tpw and  # not equal to the original value # the typed version is not junk
             lv.distance(rpw.lower(), tpw.lower()) <= TYPO_EDIT_DIST_LIMIT)

def typing_speed_vs_typo():
    tsd = []
    for row in pwresults_data():
        if not row['ttpw']:
            continue
        tspeed = get_ttime(row['ttpw'])
        if tspeed>0 and tspeed<300:
            tsd.append((tspeed, row['workerId'],
                        row['rpw'], row['tpw'], row['ttpw'], istypo(row['rpw'], row['tpw'])))
    # avg_speed = sum(x for x in tsd)/float(len(tsd))
    tsd = sorted(tsd, key=operator.itemgetter(0))
    return tsd


def typist_typo():
    T = typing_speed_vs_typo()
    W = defaultdict(list)
    for ttime, wid, rpw, tpw, ttpw, typo in T:
        ttpw = eval(ttpw)
        W[wid].append((len(ttpw), ttime, typo))

    for w,v  in W.items():
        avg_speed = sum(r[0] for r in v)/float(sum(r[1] for r in v))
        num_typo = len(filter(lambda x: x[-1], v))
        W[w] = [avg_speed, num_typo, len(v)]

    speed_arr = sorted([x[0] for x in W.values()])
    n = len(speed_arr)
    total_typed = len(T)
    for i in range(4):
        low = speed_arr[i*n/4]
        high = speed_arr[min((i+1)*n/4,n-1)]
        this_class_workers = filter(lambda x: x[0]>=low and x[0] < high, 
                                    W.values())
        avg_speed = sum(x[0] for x in this_class_workers)/float(len(this_class_workers))
        fraction_typed = sum(x[2] for x in this_class_workers)/float(total_typed)
        fraction_mistyped = sum(x[1] for x in this_class_workers)/float(total_typed)
        fraction_mistyped /= fraction_typed
        print "{},{},{},{}".format(i,int(avg_speed*60), fraction_typed, fraction_mistyped)


def popularity_typo():
    import dawg
    pwmry = dawg.IntCompletionDAWG().load('data/data_rockyou/all_rockyou.dawg')
    each_cl_count = defaultdict(int)
    for pw in get_typos():
        f = pwmry.get(pw['rpw'])
        f = 1 if f==1 else 2 if f>=2 and f<=11 else 3 if f>=12 and f<=211 else 4
        each_cl_count[f] += 1
    print each_cl_count
    each_cl_count = defaultdict(int)

    for pw in pwresults_data():
        f = pwmry.get(pw['rpw'])
        f = 1 if f==1 else 2 if f>=2 and f<=11 else 3 if f>=12 and f<=211 else 4
        each_cl_count[f] += 1
    print each_cl_count
        
    
def workers_speed(w, tsd):
    return np.average([r[0] for r in tsd if w==r[1]])


def length_vs_pwtypo():
    length_lims = [(0,5), (6,7), (8,9), (10,11), (12,25)]
    
    query = "SELECT pwid, rpw, tpw from pwresults where tpw!='' "\
      "and pwid<='pwid000100000' and char_length(rpw)>={} and char_length(rpw)<={} "\
      "{} order by rand() limit 5000;"
    wrong_pws = json.load(open('data/capslockfix.json'))
    # query = "SELECT COUNT(*) as c form (SELECT  pwid from pwresults where tpw!='' and "\
    #                              "pwid<='pwid000100000' and char_length(rpw)>={} and "\
    #                              "char_length(rpw)<={} {} order by rand() limit 5000) as T;"
    cnt = {}
    for l in length_lims:
        qstr = query.format(l[0], l[1], '')
        L = list(db.query(query.format(l[0], l[1], '')))
        for i,v in enumerate(L):
            pwid = v['pwid']
            if pwid in wrong_pws:
                v['rpw'] = wrong_pws[pwid]['rpw']
                v['tpw'] = wrong_pws[pwid]['tpw']

        cnt[l] = [len(L),
                  len(filter(lambda x: x['rpw'] != x['tpw'], L))]
    print cnt
    total = float(sum(x[0] for x in cnt.values()))
    for l in cnt:
        print l, cnt[l][0]/total, cnt[l][1]/float(cnt[l][0])

def length_vs_pwtypo2(bar_chart=False):
    rpwl_d = defaultdict(int)
    tpwl_d = defaultdict(int)
    #points = [(3, 5.828009950870701), (4, 5.837248637474159), (5, 5.873124943278318), (6, 6.223919176901894), (7, 6.497289972899729), (8, 6.926109443379508), (9, 7.445877341424987), (10, 8.696141298233773), (11, 9.24170616113744), (12, 9.859154929577464), (13, 10.619872379216044), (14, 12.112036336109009), (15, 13.72549019607843), (16, 13.368983957219251), (17, 12.955465587044534), (18, 13.559322033898304), (19, 11.363636363636363)]
    #num_pw_points = {3: 79993, 4: 79815, 5: 77131, 6: 59191, 7: 44280, 8: 27649, 9: 17137, 10: 8889, 11: 5486, 12: 3479, 13: 2194, 14: 1321, 15: 765, 16: 374, 17: 247, 18: 177, 19: 132}

    points = []
    num_pw_points = {}
    if not points:
        for row in pwresultstable['rpw', 'tpw']:
            rpw, tpw = row['rpw'], row['tpw']
            if tpw=='': continue
            n = len(rpw)
            rpwl_d[n] += 1
            if rpw.strip() != tpw.strip():
                tpwl_d[n] += 1
    
        max_len = max(rpwl_d)
        for i in xrange(3, max_len):
            num_pw = sum(v for k,v in rpwl_d.items() if k>i)
            num_typo = sum(v for k,v in tpwl_d.items() if k>i)
            if num_pw<100:
                break
            points.append((i, 100.0*num_typo/num_pw))
            num_pw_points[i] = num_pw
    
    print points
    print num_pw_points
    total_number_of_pw = sum(rpwl_d.values())/100.0
    total_number_of_typos = sum(tpwl_d.values())/100.0
    fig, ax1 = plt.subplots()
    if bar_chart:
        length_ranges = [(0,5), (6, 7), (8, 9), (10, 11), (12, 100)]
        X = np.arange(0, len(length_ranges))
        Y = [0 for _ in length_ranges]
        Y1 = [0 for _ in length_ranges]
        for i, t in enumerate(length_ranges):
            l, h = t
            Y1[i] = sum(v for k,v in rpwl_d.items() if k>=l and k<=h)
            Y[i] = sum(v for k,v in tpwl_d.items() if k>=l and k<=h)*100.0/Y1[i]
            Y1[i] /= total_number_of_pw
        print zip(X, Y)
        print zip(X, Y1)
        Xticks = [r"$%d-%d$" % (l[0], l[1]) for l in length_ranges]
        Xticks[0] = r"$\leq %d$" % length_ranges[0][1]
        Xticks[-1] = r"$\geq %d$" % length_ranges[-1][0]
        width = 0.2
        plt.bar(range(len(length_ranges)), Y, width=2*width, color='blue')
        ax1.set_xticks(X+width)
        ax1.set_xticklabels(Xticks, fontsize=14)
        ax1.legend("Fraction of sample")
        plt.ylim(0, 15)
        ax = ax1.twinx()
        ax.plot(X+width, Y1, 'ro-')
        plt.ylim(0, 50)
        plt.xlim(-0.5, len(length_ranges))
    else:
        X, Y = np.array(points).T
        plt.plot(X, Y)
    ax1.set_xlabel(r'Length of passwords', fontsize=14)
    ax1.set_ylabel(r'Fraction of typos $(%)$', fontsize=14)
    ax.set_ylabel(r'Fraction of passwords $(%)$', fontsize=14)
    plt.savefig('plots/length_typo.pdf')

def fix_for_cplockerr():
    override_pws = {}
    for line in open('err_pws'):
        try:
            pwid, rpw, tpw = line.split(',',2)
        except:
            print "ERRRRRRRRRR", line
            exit(0)
        row = db.query("select * from pwresults where pwid='{}'".format(pwid)).next()
        row['rpw']  = rpw
        row['tpw']  = tpw
        override_pws[pwid] = row
    json.dump(override_pws, open('data/capslockfix.json', 'w'), indent=2)



def turker_vs_typo():
    query_str_all = "SELECT workerId, count(*) as count from pwresults group by workerId"
    query_str_typo = "SELECT workerId, count(*) as count from pwresults where tpw!='' "\
                     "and rpw collate latin1_general_cs != tpw group by workerId"
    print query_str_typo
    worker_all_count = dict((r['workerId'], r['count']) for r in db.query(query_str_all))
    worker_typo_count = dict((r['workerId'], r['count']) for r in db.query(query_str_typo))
    

    tworkers = defaultdict(list)
    for pwid, v in json.load(open('data/capslockfix.json')).items():
        rpw, tpw, wid = v['rpw'], v['tpw'], v['workerId']
        tworkers[wid].append((rpw,tpw))

    for w, v in tworkers.items():
        worker_all_count[w] -= len(v)
        if w in worker_typo_count:
            worker_typo_count[w] -= len(filter(lambda x: x[0] != x[1], v))
    print "At least one:", len(filter(lambda v: v>0, worker_typo_count.values()))
    print "more than 5", len(filter(lambda v: v>5, worker_typo_count.values()))
    print "Total worker", len(worker_all_count)
    return

    for d,v in worker_all_count.items():
        worker_typo_count[d] = worker_typo_count.get(d, 0.0)*100/float(v)
    print worker_typo_count.items()[:10]
    points = []
    total_num_worker = float(len(worker_all_count))
    last = 0 
    for i in xrange(0, 101, 10):
        points.append((i, len(filter(lambda x: x>=i,
                                     worker_typo_count.values()))))
        last = i
    print points
    fig, ax1 = plt.subplots()
    ax1.set_xlabel(r'Fraction of typos (%)', fontsize=14)
    ax1.set_ylabel(r'Number of workers', fontsize=14)
    X, Y = np.array(points).T    
    width = 5
    bar = ax1.bar(X-width, Y, 2*width, color='#dddddd')
    ax1.set_xticks(X)
    #plt.xlim(-1, 100)
    plt.savefig('plots/worker_typo.pdf')


type_regex_ = re.compile(r'(?P<U>[A-Z]+)|(?P<L>[a-z]+)|(?P<D>[0-9]+)|(?P<S>\W+)')
def get_complexity_cohort(pw):
    return len(filter(lambda x: any(x), zip(*type_regex_.findall(pw))))

def group_arr(arr):
    adict = defaultdict(int)
    for a in arr:
        adict[a] += 1
    return adict

def complexity_vs_typo():
    comp_cl_total = defaultdict(int)
    comp_cl_typo  = defaultdict(int)
    long_pw = defaultdict(list)
    for row in pwresults_data(type_='complex'):
        rpw, tpw = row['rpw'], row['tpw']
        if not tpw: continue
        istypo = (rpw.strip() != tpw.strip())
        c = get_complexity_cohort(rpw)
        comp_cl_total[c] += 1
        if istypo:
            comp_cl_typo[c] += 1
        long_pw[c].append(len(rpw))
    total_f = sum(comp_cl_total.values())/100.0
    total_t = sum(comp_cl_typo.values())/100.0
    print comp_cl_total, comp_cl_typo
    long_pw = {k: group_arr(v) for k,v in long_pw.items()}
    print "Long pw: ", long_pw
    for i in xrange(1, 5):
        print ','.join([str(i), str(comp_cl_total[i]/total_f), str(comp_cl_typo[i]*100.0/comp_cl_total[i])])


def freq_vs_typo():
    for row in dbaccess.get_typos():
        pass


def edist_vs_typo():
    edist_map = defaultdict(int)
    for row in pwresultstable['rpw', 'tpw']:
        rpw, tpw = row['rpw'], row['tpw']
        if not tpw.strip():
            continue
        if rpw==tpw:
            d = 0
        elif rpw.swapcase() == tpw or rpw.title() == tpw:
            d = 1
        else:
            d = DLdist(rpw.lower(), tpw.lower())
        edist_map[d] += 1
    total = float(sum(edist_map.values())-edist_map[0])/100.0
    print total
    print max(edist_map)
    points = [(i, sum(v for k,v in edist_map.items() if k==i)/total)
              for i in xrange(1, max(edist_map))]
    

    print points
    fig, ax1 = plt.subplots()
    ax1.set_xlabel(r'Edit distance', fontsize=14)
    ax1.set_ylabel(r'Fraction of typos (%)', fontsize=14)
    X, Y = zip(*points)
    ax1.plot(X, Y, 'o-')
    plt.savefig('plots/edist_typo.pdf')
